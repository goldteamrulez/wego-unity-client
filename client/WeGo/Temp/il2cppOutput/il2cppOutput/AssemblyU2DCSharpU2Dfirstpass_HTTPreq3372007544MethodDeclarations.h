﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HTTPreq
struct HTTPreq_t3372007544;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HTTPreq::.ctor()
extern "C"  void HTTPreq__ctor_m1329710387 (HTTPreq_t3372007544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HTTPreq::Start()
extern "C"  void HTTPreq_Start_m3797058471 (HTTPreq_t3372007544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HTTPreq::GetText()
extern "C"  Il2CppObject * HTTPreq_GetText_m1041910378 (HTTPreq_t3372007544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HTTPreq::Upload(System.String,System.String,System.String,System.String)
extern "C"  Il2CppObject * HTTPreq_Upload_m3931106518 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___lat1, String_t* ___lon2, String_t* ___extension3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
