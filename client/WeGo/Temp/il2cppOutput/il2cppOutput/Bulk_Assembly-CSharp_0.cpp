﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// GoogleMap
struct GoogleMap_t686614771;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// GoogleMap/<_Refresh>c__Iterator0
struct U3C_RefreshU3Ec__Iterator0_t337046165;
// UnityEngine.Renderer
struct Renderer_t257310565;
// System.Object
struct Il2CppObject;
// GoogleMapLocation
struct GoogleMapLocation_t1586030896;
// GoogleMapMarker
struct GoogleMapMarker_t2438220333;
// GoogleMapPath
struct GoogleMapPath_t3384754880;
// SpinningCube
struct SpinningCube_t4269489377;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMap686614771.h"
#include "AssemblyU2DCSharp_GoogleMap686614771MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_GoogleMapMarker2438220333.h"
#include "AssemblyU2DCSharp_GoogleMapPath3384754880.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_GoogleMap_U3C_RefreshU3Ec__Iterat337046165MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMap_U3C_RefreshU3Ec__Iterat337046165.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LocationService1617852714MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "AssemblyU2DCSharp_GoogleMapLocation1586030896.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMap_MapType679558006.h"
#include "UnityEngine_UnityEngine_LocationService1617852714.h"
#include "UnityEngine_UnityEngine_LocationServiceStatus2482073234.h"
#include "AssemblyU2DCSharp_GoogleMapMarker_GoogleMapMarkerS3913171074.h"
#include "AssemblyU2DCSharp_GoogleMapColor1743004618.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_GoogleMap_MapType679558006MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMapColor1743004618MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMapLocation1586030896MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMapMarker2438220333MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMapMarker_GoogleMapMarkerS3913171074MethodDeclarations.h"
#include "AssemblyU2DCSharp_GoogleMapPath3384754880MethodDeclarations.h"
#include "AssemblyU2DCSharp_SpinningCube4269489377.h"
#include "AssemblyU2DCSharp_SpinningCube4269489377MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t257310565_m772028041(__this, method) ((  Renderer_t257310565 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GoogleMap::.ctor()
extern "C"  void GoogleMap__ctor_m3466744138 (GoogleMap_t686614771 * __this, const MethodInfo* method)
{
	{
		__this->set_loadOnStart_2((bool)1);
		__this->set_autoLocateCenter_3((bool)1);
		__this->set_zoom_5(((int32_t)13));
		__this->set_size_7(((int32_t)512));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMap::Start()
extern "C"  void GoogleMap_Start_m3269610766 (GoogleMap_t686614771 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_loadOnStart_2();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		GoogleMap_Refresh_m3456073191(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void GoogleMap::Refresh()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral844883549;
extern const uint32_t GoogleMap_Refresh_m3456073191_MetadataUsageId;
extern "C"  void GoogleMap_Refresh_m3456073191 (GoogleMap_t686614771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GoogleMap_Refresh_m3456073191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_autoLocateCenter_3();
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		GoogleMapMarkerU5BU5D_t100527744* L_1 = __this->get_markers_9();
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))
		{
			goto IL_002f;
		}
	}
	{
		GoogleMapPathU5BU5D_t3462109761* L_2 = __this->get_paths_10();
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))
		{
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral844883549, /*hidden argument*/NULL);
	}

IL_002f:
	{
		Il2CppObject * L_3 = GoogleMap__Refresh_m2373120700(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator GoogleMap::_Refresh()
extern Il2CppClass* U3C_RefreshU3Ec__Iterator0_t337046165_il2cpp_TypeInfo_var;
extern const uint32_t GoogleMap__Refresh_m2373120700_MetadataUsageId;
extern "C"  Il2CppObject * GoogleMap__Refresh_m2373120700 (GoogleMap_t686614771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GoogleMap__Refresh_m2373120700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3C_RefreshU3Ec__Iterator0_t337046165 * V_0 = NULL;
	{
		U3C_RefreshU3Ec__Iterator0_t337046165 * L_0 = (U3C_RefreshU3Ec__Iterator0_t337046165 *)il2cpp_codegen_object_new(U3C_RefreshU3Ec__Iterator0_t337046165_il2cpp_TypeInfo_var);
		U3C_RefreshU3Ec__Iterator0__ctor_m576093384(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3C_RefreshU3Ec__Iterator0_t337046165 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_8(__this);
		U3C_RefreshU3Ec__Iterator0_t337046165 * L_2 = V_0;
		return L_2;
	}
}
// System.Void GoogleMap/<_Refresh>c__Iterator0::.ctor()
extern "C"  void U3C_RefreshU3Ec__Iterator0__ctor_m576093384 (U3C_RefreshU3Ec__Iterator0_t337046165 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GoogleMap/<_Refresh>c__Iterator0::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* MapType_t679558006_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* GoogleMapMarkerSize_t3913171074_il2cpp_TypeInfo_var;
extern Il2CppClass* GoogleMapColor_t1743004618_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m772028041_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4270338618;
extern Il2CppCodeGenString* _stringLiteral1334054574;
extern Il2CppCodeGenString* _stringLiteral3076720493;
extern Il2CppCodeGenString* _stringLiteral1960745698;
extern Il2CppCodeGenString* _stringLiteral2334078884;
extern Il2CppCodeGenString* _stringLiteral1968398454;
extern Il2CppCodeGenString* _stringLiteral3002739339;
extern Il2CppCodeGenString* _stringLiteral372029328;
extern Il2CppCodeGenString* _stringLiteral372029325;
extern Il2CppCodeGenString* _stringLiteral93847055;
extern Il2CppCodeGenString* _stringLiteral1030189529;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral3790137314;
extern Il2CppCodeGenString* _stringLiteral1824934065;
extern Il2CppCodeGenString* _stringLiteral372029394;
extern Il2CppCodeGenString* _stringLiteral3614737590;
extern Il2CppCodeGenString* _stringLiteral3607987508;
extern Il2CppCodeGenString* _stringLiteral3718130048;
extern Il2CppCodeGenString* _stringLiteral372029331;
extern const uint32_t U3C_RefreshU3Ec__Iterator0_MoveNext_m1320191108_MetadataUsageId;
extern "C"  bool U3C_RefreshU3Ec__Iterator0_MoveNext_m1320191108 (U3C_RefreshU3Ec__Iterator0_t337046165 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3C_RefreshU3Ec__Iterator0_MoveNext_m1320191108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	GoogleMapMarker_t2438220333 * V_1 = NULL;
	GoogleMapLocation_t1586030896 * V_2 = NULL;
	GoogleMapLocationU5BU5D_t2073025553* V_3 = NULL;
	int32_t V_4 = 0;
	GoogleMapPath_t3384754880 * V_5 = NULL;
	GoogleMapLocation_t1586030896 * V_6 = NULL;
	GoogleMapLocationU5BU5D_t2073025553* V_7 = NULL;
	int32_t V_8 = 0;
	String_t* G_B9_0 = NULL;
	String_t* G_B9_1 = NULL;
	U3C_RefreshU3Ec__Iterator0_t337046165 * G_B9_2 = NULL;
	String_t* G_B8_0 = NULL;
	String_t* G_B8_1 = NULL;
	U3C_RefreshU3Ec__Iterator0_t337046165 * G_B8_2 = NULL;
	String_t* G_B10_0 = NULL;
	String_t* G_B10_1 = NULL;
	String_t* G_B10_2 = NULL;
	U3C_RefreshU3Ec__Iterator0_t337046165 * G_B10_3 = NULL;
	U3C_RefreshU3Ec__Iterator0_t337046165 * G_B12_0 = NULL;
	U3C_RefreshU3Ec__Iterator0_t337046165 * G_B11_0 = NULL;
	int32_t G_B13_0 = 0;
	U3C_RefreshU3Ec__Iterator0_t337046165 * G_B13_1 = NULL;
	String_t* G_B15_0 = NULL;
	String_t* G_B15_1 = NULL;
	U3C_RefreshU3Ec__Iterator0_t337046165 * G_B15_2 = NULL;
	String_t* G_B14_0 = NULL;
	String_t* G_B14_1 = NULL;
	U3C_RefreshU3Ec__Iterator0_t337046165 * G_B14_2 = NULL;
	String_t* G_B16_0 = NULL;
	String_t* G_B16_1 = NULL;
	String_t* G_B16_2 = NULL;
	U3C_RefreshU3Ec__Iterator0_t337046165 * G_B16_3 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_11();
		V_0 = L_0;
		__this->set_U24PC_11((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_04cc;
		}
	}
	{
		goto IL_04f3;
	}

IL_0021:
	{
		__this->set_U3CurlU3E__0_0(_stringLiteral4270338618);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_U3CqsU3E__1_1(L_2);
		GoogleMap_t686614771 * L_3 = __this->get_U24this_8();
		NullCheck(L_3);
		bool L_4 = L_3->get_autoLocateCenter_3();
		if (L_4)
		{
			goto IL_0111;
		}
	}
	{
		GoogleMap_t686614771 * L_5 = __this->get_U24this_8();
		NullCheck(L_5);
		GoogleMapLocation_t1586030896 * L_6 = L_5->get_centerLocation_4();
		NullCheck(L_6);
		String_t* L_7 = L_6->get_address_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_9 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0096;
		}
	}
	{
		String_t* L_10 = __this->get_U3CqsU3E__1_1();
		GoogleMap_t686614771 * L_11 = __this->get_U24this_8();
		NullCheck(L_11);
		GoogleMapLocation_t1586030896 * L_12 = L_11->get_centerLocation_4();
		NullCheck(L_12);
		String_t* L_13 = L_12->get_address_0();
		String_t* L_14 = WWW_UnEscapeURL_m1911584158(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m612901809(NULL /*static, unused*/, L_10, _stringLiteral1334054574, L_14, /*hidden argument*/NULL);
		__this->set_U3CqsU3E__1_1(L_15);
		goto IL_00e5;
	}

IL_0096:
	{
		String_t* L_16 = __this->get_U3CqsU3E__1_1();
		GoogleMap_t686614771 * L_17 = __this->get_U24this_8();
		NullCheck(L_17);
		GoogleMapLocation_t1586030896 * L_18 = L_17->get_centerLocation_4();
		NullCheck(L_18);
		float L_19 = L_18->get_latitude_1();
		float L_20 = L_19;
		Il2CppObject * L_21 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_20);
		GoogleMap_t686614771 * L_22 = __this->get_U24this_8();
		NullCheck(L_22);
		GoogleMapLocation_t1586030896 * L_23 = L_22->get_centerLocation_4();
		NullCheck(L_23);
		float L_24 = L_23->get_longitude_2();
		float L_25 = L_24;
		Il2CppObject * L_26 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_25);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3076720493, L_21, L_26, /*hidden argument*/NULL);
		String_t* L_28 = WWW_UnEscapeURL_m1911584158(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		String_t* L_29 = String_Concat_m612901809(NULL /*static, unused*/, L_16, _stringLiteral1334054574, L_28, /*hidden argument*/NULL);
		__this->set_U3CqsU3E__1_1(L_29);
	}

IL_00e5:
	{
		String_t* L_30 = __this->get_U3CqsU3E__1_1();
		GoogleMap_t686614771 * L_31 = __this->get_U24this_8();
		NullCheck(L_31);
		int32_t* L_32 = L_31->get_address_of_zoom_5();
		String_t* L_33 = Int32_ToString_m2960866144(L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m612901809(NULL /*static, unused*/, L_30, _stringLiteral1960745698, L_33, /*hidden argument*/NULL);
		__this->set_U3CqsU3E__1_1(L_34);
	}

IL_0111:
	{
		String_t* L_35 = __this->get_U3CqsU3E__1_1();
		GoogleMap_t686614771 * L_36 = __this->get_U24this_8();
		NullCheck(L_36);
		int32_t L_37 = L_36->get_size_7();
		int32_t L_38 = L_37;
		Il2CppObject * L_39 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_38);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_40 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1968398454, L_39, /*hidden argument*/NULL);
		String_t* L_41 = WWW_UnEscapeURL_m1911584158(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		String_t* L_42 = String_Concat_m612901809(NULL /*static, unused*/, L_35, _stringLiteral2334078884, L_41, /*hidden argument*/NULL);
		__this->set_U3CqsU3E__1_1(L_42);
		String_t* L_43 = __this->get_U3CqsU3E__1_1();
		GoogleMap_t686614771 * L_44 = __this->get_U24this_8();
		NullCheck(L_44);
		bool L_45 = L_44->get_doubleResolution_8();
		G_B8_0 = _stringLiteral3002739339;
		G_B8_1 = L_43;
		G_B8_2 = __this;
		if (!L_45)
		{
			G_B9_0 = _stringLiteral3002739339;
			G_B9_1 = L_43;
			G_B9_2 = __this;
			goto IL_016c;
		}
	}
	{
		G_B10_0 = _stringLiteral372029328;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		G_B10_3 = G_B8_2;
		goto IL_0171;
	}

IL_016c:
	{
		G_B10_0 = _stringLiteral372029325;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
		G_B10_3 = G_B9_2;
	}

IL_0171:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_46 = String_Concat_m612901809(NULL /*static, unused*/, G_B10_2, G_B10_1, G_B10_0, /*hidden argument*/NULL);
		NullCheck(G_B10_3);
		G_B10_3->set_U3CqsU3E__1_1(L_46);
		String_t* L_47 = __this->get_U3CqsU3E__1_1();
		GoogleMap_t686614771 * L_48 = __this->get_U24this_8();
		NullCheck(L_48);
		int32_t* L_49 = L_48->get_address_of_mapType_6();
		Il2CppObject * L_50 = Box(MapType_t679558006_il2cpp_TypeInfo_var, L_49);
		NullCheck(L_50);
		String_t* L_51 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		NullCheck(L_51);
		String_t* L_52 = String_ToLower_m2994460523(L_51, /*hidden argument*/NULL);
		String_t* L_53 = String_Concat_m612901809(NULL /*static, unused*/, L_47, _stringLiteral93847055, L_52, /*hidden argument*/NULL);
		__this->set_U3CqsU3E__1_1(L_53);
		__this->set_U3CusingSensorU3E__2_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_54 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_54);
		bool L_55 = LocationService_get_isEnabledByUser_m840009485(L_54, /*hidden argument*/NULL);
		G_B11_0 = __this;
		if (!L_55)
		{
			G_B12_0 = __this;
			goto IL_01d2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_56 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_56);
		int32_t L_57 = LocationService_get_status_m1865246926(L_56, /*hidden argument*/NULL);
		G_B13_0 = ((((int32_t)L_57) == ((int32_t)2))? 1 : 0);
		G_B13_1 = G_B11_0;
		goto IL_01d3;
	}

IL_01d2:
	{
		G_B13_0 = 0;
		G_B13_1 = G_B12_0;
	}

IL_01d3:
	{
		NullCheck(G_B13_1);
		G_B13_1->set_U3CusingSensorU3E__2_2((bool)G_B13_0);
		String_t* L_58 = __this->get_U3CqsU3E__1_1();
		bool L_59 = __this->get_U3CusingSensorU3E__2_2();
		G_B14_0 = _stringLiteral1030189529;
		G_B14_1 = L_58;
		G_B14_2 = __this;
		if (!L_59)
		{
			G_B15_0 = _stringLiteral1030189529;
			G_B15_1 = L_58;
			G_B15_2 = __this;
			goto IL_01f9;
		}
	}
	{
		G_B16_0 = _stringLiteral3323263070;
		G_B16_1 = G_B14_0;
		G_B16_2 = G_B14_1;
		G_B16_3 = G_B14_2;
		goto IL_01fe;
	}

IL_01f9:
	{
		G_B16_0 = _stringLiteral2609877245;
		G_B16_1 = G_B15_0;
		G_B16_2 = G_B15_1;
		G_B16_3 = G_B15_2;
	}

IL_01fe:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = String_Concat_m612901809(NULL /*static, unused*/, G_B16_2, G_B16_1, G_B16_0, /*hidden argument*/NULL);
		NullCheck(G_B16_3);
		G_B16_3->set_U3CqsU3E__1_1(L_60);
		GoogleMap_t686614771 * L_61 = __this->get_U24this_8();
		NullCheck(L_61);
		GoogleMapMarkerU5BU5D_t100527744* L_62 = L_61->get_markers_9();
		__this->set_U24locvar0_3(L_62);
		__this->set_U24locvar1_4(0);
		goto IL_0322;
	}

IL_0225:
	{
		GoogleMapMarkerU5BU5D_t100527744* L_63 = __this->get_U24locvar0_3();
		int32_t L_64 = __this->get_U24locvar1_4();
		NullCheck(L_63);
		int32_t L_65 = L_64;
		GoogleMapMarker_t2438220333 * L_66 = (L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		V_1 = L_66;
		String_t* L_67 = __this->get_U3CqsU3E__1_1();
		GoogleMapMarker_t2438220333 * L_68 = V_1;
		NullCheck(L_68);
		int32_t* L_69 = L_68->get_address_of_size_0();
		Il2CppObject * L_70 = Box(GoogleMapMarkerSize_t3913171074_il2cpp_TypeInfo_var, L_69);
		NullCheck(L_70);
		String_t* L_71 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		NullCheck(L_71);
		String_t* L_72 = String_ToLower_m2994460523(L_71, /*hidden argument*/NULL);
		GoogleMapMarker_t2438220333 * L_73 = V_1;
		NullCheck(L_73);
		int32_t L_74 = L_73->get_color_1();
		int32_t L_75 = L_74;
		Il2CppObject * L_76 = Box(GoogleMapColor_t1743004618_il2cpp_TypeInfo_var, &L_75);
		GoogleMapMarker_t2438220333 * L_77 = V_1;
		NullCheck(L_77);
		String_t* L_78 = L_77->get_label_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_79 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral1824934065, L_72, L_76, L_78, /*hidden argument*/NULL);
		String_t* L_80 = String_Concat_m612901809(NULL /*static, unused*/, L_67, _stringLiteral3790137314, L_79, /*hidden argument*/NULL);
		__this->set_U3CqsU3E__1_1(L_80);
		GoogleMapMarker_t2438220333 * L_81 = V_1;
		NullCheck(L_81);
		GoogleMapLocationU5BU5D_t2073025553* L_82 = L_81->get_locations_3();
		V_3 = L_82;
		V_4 = 0;
		goto IL_030a;
	}

IL_0289:
	{
		GoogleMapLocationU5BU5D_t2073025553* L_83 = V_3;
		int32_t L_84 = V_4;
		NullCheck(L_83);
		int32_t L_85 = L_84;
		GoogleMapLocation_t1586030896 * L_86 = (L_83)->GetAt(static_cast<il2cpp_array_size_t>(L_85));
		V_2 = L_86;
		GoogleMapLocation_t1586030896 * L_87 = V_2;
		NullCheck(L_87);
		String_t* L_88 = L_87->get_address_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_89 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_90 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_88, L_89, /*hidden argument*/NULL);
		if (!L_90)
		{
			goto IL_02c9;
		}
	}
	{
		String_t* L_91 = __this->get_U3CqsU3E__1_1();
		GoogleMapLocation_t1586030896 * L_92 = V_2;
		NullCheck(L_92);
		String_t* L_93 = L_92->get_address_0();
		String_t* L_94 = WWW_UnEscapeURL_m1911584158(NULL /*static, unused*/, L_93, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_95 = String_Concat_m612901809(NULL /*static, unused*/, L_91, _stringLiteral372029394, L_94, /*hidden argument*/NULL);
		__this->set_U3CqsU3E__1_1(L_95);
		goto IL_0304;
	}

IL_02c9:
	{
		String_t* L_96 = __this->get_U3CqsU3E__1_1();
		GoogleMapLocation_t1586030896 * L_97 = V_2;
		NullCheck(L_97);
		float L_98 = L_97->get_latitude_1();
		float L_99 = L_98;
		Il2CppObject * L_100 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_99);
		GoogleMapLocation_t1586030896 * L_101 = V_2;
		NullCheck(L_101);
		float L_102 = L_101->get_longitude_2();
		float L_103 = L_102;
		Il2CppObject * L_104 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_103);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_105 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3076720493, L_100, L_104, /*hidden argument*/NULL);
		String_t* L_106 = WWW_UnEscapeURL_m1911584158(NULL /*static, unused*/, L_105, /*hidden argument*/NULL);
		String_t* L_107 = String_Concat_m612901809(NULL /*static, unused*/, L_96, _stringLiteral372029394, L_106, /*hidden argument*/NULL);
		__this->set_U3CqsU3E__1_1(L_107);
	}

IL_0304:
	{
		int32_t L_108 = V_4;
		V_4 = ((int32_t)((int32_t)L_108+(int32_t)1));
	}

IL_030a:
	{
		int32_t L_109 = V_4;
		GoogleMapLocationU5BU5D_t2073025553* L_110 = V_3;
		NullCheck(L_110);
		if ((((int32_t)L_109) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_110)->max_length)))))))
		{
			goto IL_0289;
		}
	}
	{
		int32_t L_111 = __this->get_U24locvar1_4();
		__this->set_U24locvar1_4(((int32_t)((int32_t)L_111+(int32_t)1)));
	}

IL_0322:
	{
		int32_t L_112 = __this->get_U24locvar1_4();
		GoogleMapMarkerU5BU5D_t100527744* L_113 = __this->get_U24locvar0_3();
		NullCheck(L_113);
		if ((((int32_t)L_112) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_113)->max_length)))))))
		{
			goto IL_0225;
		}
	}
	{
		GoogleMap_t686614771 * L_114 = __this->get_U24this_8();
		NullCheck(L_114);
		GoogleMapPathU5BU5D_t3462109761* L_115 = L_114->get_paths_10();
		__this->set_U24locvar4_5(L_115);
		__this->set_U24locvar5_6(0);
		goto IL_0478;
	}

IL_0352:
	{
		GoogleMapPathU5BU5D_t3462109761* L_116 = __this->get_U24locvar4_5();
		int32_t L_117 = __this->get_U24locvar5_6();
		NullCheck(L_116);
		int32_t L_118 = L_117;
		GoogleMapPath_t3384754880 * L_119 = (L_116)->GetAt(static_cast<il2cpp_array_size_t>(L_118));
		V_5 = L_119;
		String_t* L_120 = __this->get_U3CqsU3E__1_1();
		GoogleMapPath_t3384754880 * L_121 = V_5;
		NullCheck(L_121);
		int32_t L_122 = L_121->get_weight_0();
		int32_t L_123 = L_122;
		Il2CppObject * L_124 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_123);
		GoogleMapPath_t3384754880 * L_125 = V_5;
		NullCheck(L_125);
		int32_t L_126 = L_125->get_color_1();
		int32_t L_127 = L_126;
		Il2CppObject * L_128 = Box(GoogleMapColor_t1743004618_il2cpp_TypeInfo_var, &L_127);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_129 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3607987508, L_124, L_128, /*hidden argument*/NULL);
		String_t* L_130 = String_Concat_m612901809(NULL /*static, unused*/, L_120, _stringLiteral3614737590, L_129, /*hidden argument*/NULL);
		__this->set_U3CqsU3E__1_1(L_130);
		GoogleMapPath_t3384754880 * L_131 = V_5;
		NullCheck(L_131);
		bool L_132 = L_131->get_fill_2();
		if (!L_132)
		{
			goto IL_03c7;
		}
	}
	{
		String_t* L_133 = __this->get_U3CqsU3E__1_1();
		GoogleMapPath_t3384754880 * L_134 = V_5;
		NullCheck(L_134);
		int32_t L_135 = L_134->get_fillColor_3();
		int32_t L_136 = L_135;
		Il2CppObject * L_137 = Box(GoogleMapColor_t1743004618_il2cpp_TypeInfo_var, &L_136);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_138 = String_Concat_m2000667605(NULL /*static, unused*/, L_133, _stringLiteral3718130048, L_137, /*hidden argument*/NULL);
		__this->set_U3CqsU3E__1_1(L_138);
	}

IL_03c7:
	{
		GoogleMapPath_t3384754880 * L_139 = V_5;
		NullCheck(L_139);
		GoogleMapLocationU5BU5D_t2073025553* L_140 = L_139->get_locations_4();
		V_7 = L_140;
		V_8 = 0;
		goto IL_045f;
	}

IL_03d8:
	{
		GoogleMapLocationU5BU5D_t2073025553* L_141 = V_7;
		int32_t L_142 = V_8;
		NullCheck(L_141);
		int32_t L_143 = L_142;
		GoogleMapLocation_t1586030896 * L_144 = (L_141)->GetAt(static_cast<il2cpp_array_size_t>(L_143));
		V_6 = L_144;
		GoogleMapLocation_t1586030896 * L_145 = V_6;
		NullCheck(L_145);
		String_t* L_146 = L_145->get_address_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_147 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_148 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_146, L_147, /*hidden argument*/NULL);
		if (!L_148)
		{
			goto IL_041c;
		}
	}
	{
		String_t* L_149 = __this->get_U3CqsU3E__1_1();
		GoogleMapLocation_t1586030896 * L_150 = V_6;
		NullCheck(L_150);
		String_t* L_151 = L_150->get_address_0();
		String_t* L_152 = WWW_UnEscapeURL_m1911584158(NULL /*static, unused*/, L_151, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_153 = String_Concat_m612901809(NULL /*static, unused*/, L_149, _stringLiteral372029394, L_152, /*hidden argument*/NULL);
		__this->set_U3CqsU3E__1_1(L_153);
		goto IL_0459;
	}

IL_041c:
	{
		String_t* L_154 = __this->get_U3CqsU3E__1_1();
		GoogleMapLocation_t1586030896 * L_155 = V_6;
		NullCheck(L_155);
		float L_156 = L_155->get_latitude_1();
		float L_157 = L_156;
		Il2CppObject * L_158 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_157);
		GoogleMapLocation_t1586030896 * L_159 = V_6;
		NullCheck(L_159);
		float L_160 = L_159->get_longitude_2();
		float L_161 = L_160;
		Il2CppObject * L_162 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_161);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_163 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3076720493, L_158, L_162, /*hidden argument*/NULL);
		String_t* L_164 = WWW_UnEscapeURL_m1911584158(NULL /*static, unused*/, L_163, /*hidden argument*/NULL);
		String_t* L_165 = String_Concat_m612901809(NULL /*static, unused*/, L_154, _stringLiteral372029394, L_164, /*hidden argument*/NULL);
		__this->set_U3CqsU3E__1_1(L_165);
	}

IL_0459:
	{
		int32_t L_166 = V_8;
		V_8 = ((int32_t)((int32_t)L_166+(int32_t)1));
	}

IL_045f:
	{
		int32_t L_167 = V_8;
		GoogleMapLocationU5BU5D_t2073025553* L_168 = V_7;
		NullCheck(L_168);
		if ((((int32_t)L_167) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_168)->max_length)))))))
		{
			goto IL_03d8;
		}
	}
	{
		int32_t L_169 = __this->get_U24locvar5_6();
		__this->set_U24locvar5_6(((int32_t)((int32_t)L_169+(int32_t)1)));
	}

IL_0478:
	{
		int32_t L_170 = __this->get_U24locvar5_6();
		GoogleMapPathU5BU5D_t3462109761* L_171 = __this->get_U24locvar4_5();
		NullCheck(L_171);
		if ((((int32_t)L_170) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_171)->max_length)))))))
		{
			goto IL_0352;
		}
	}
	{
		String_t* L_172 = __this->get_U3CurlU3E__0_0();
		String_t* L_173 = __this->get_U3CqsU3E__1_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_174 = String_Concat_m612901809(NULL /*static, unused*/, L_172, _stringLiteral372029331, L_173, /*hidden argument*/NULL);
		WWW_t2919945039 * L_175 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_175, L_174, /*hidden argument*/NULL);
		__this->set_U3CreqU3E__3_7(L_175);
		WWW_t2919945039 * L_176 = __this->get_U3CreqU3E__3_7();
		__this->set_U24current_9(L_176);
		bool L_177 = __this->get_U24disposing_10();
		if (L_177)
		{
			goto IL_04c7;
		}
	}
	{
		__this->set_U24PC_11(1);
	}

IL_04c7:
	{
		goto IL_04f5;
	}

IL_04cc:
	{
		GoogleMap_t686614771 * L_178 = __this->get_U24this_8();
		NullCheck(L_178);
		Renderer_t257310565 * L_179 = Component_GetComponent_TisRenderer_t257310565_m772028041(L_178, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m772028041_MethodInfo_var);
		NullCheck(L_179);
		Material_t193706927 * L_180 = Renderer_get_material_m2553789785(L_179, /*hidden argument*/NULL);
		WWW_t2919945039 * L_181 = __this->get_U3CreqU3E__3_7();
		NullCheck(L_181);
		Texture2D_t3542995729 * L_182 = WWW_get_texture_m1121178301(L_181, /*hidden argument*/NULL);
		NullCheck(L_180);
		Material_set_mainTexture_m3584203343(L_180, L_182, /*hidden argument*/NULL);
		__this->set_U24PC_11((-1));
	}

IL_04f3:
	{
		return (bool)0;
	}

IL_04f5:
	{
		return (bool)1;
	}
}
// System.Object GoogleMap/<_Refresh>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3C_RefreshU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m861895336 (U3C_RefreshU3Ec__Iterator0_t337046165 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_9();
		return L_0;
	}
}
// System.Object GoogleMap/<_Refresh>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3C_RefreshU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4152792112 (U3C_RefreshU3Ec__Iterator0_t337046165 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_9();
		return L_0;
	}
}
// System.Void GoogleMap/<_Refresh>c__Iterator0::Dispose()
extern "C"  void U3C_RefreshU3Ec__Iterator0_Dispose_m26269983 (U3C_RefreshU3Ec__Iterator0_t337046165 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_10((bool)1);
		__this->set_U24PC_11((-1));
		return;
	}
}
// System.Void GoogleMap/<_Refresh>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3C_RefreshU3Ec__Iterator0_Reset_m2906286153_MetadataUsageId;
extern "C"  void U3C_RefreshU3Ec__Iterator0_Reset_m2906286153 (U3C_RefreshU3Ec__Iterator0_t337046165 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3C_RefreshU3Ec__Iterator0_Reset_m2906286153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GoogleMapLocation::.ctor()
extern "C"  void GoogleMapLocation__ctor_m1695161839 (GoogleMapLocation_t1586030896 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMapMarker::.ctor()
extern "C"  void GoogleMapMarker__ctor_m2788975012 (GoogleMapMarker_t2438220333 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoogleMapPath::.ctor()
extern "C"  void GoogleMapPath__ctor_m1908538751 (GoogleMapPath_t3384754880 * __this, const MethodInfo* method)
{
	{
		__this->set_weight_0(5);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpinningCube::.ctor()
extern "C"  void SpinningCube__ctor_m2246610738 (SpinningCube_t4269489377 * __this, const MethodInfo* method)
{
	{
		__this->set_m_Speed_2((20.0f));
		Vector3_t2243707580  L_0 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_RotationDirection_3(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpinningCube::ToggleRotationDirection()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4035899708;
extern const uint32_t SpinningCube_ToggleRotationDirection_m2472367075_MetadataUsageId;
extern "C"  void SpinningCube_ToggleRotationDirection_m2472367075 (SpinningCube_t4269489377 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpinningCube_ToggleRotationDirection_m2472367075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral4035899708, /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = __this->get_m_RotationDirection_3();
		Vector3_t2243707580  L_1 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Vector3_t2243707580  L_3 = Vector3_get_down_m2372302126(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_RotationDirection_3(L_3);
		goto IL_003a;
	}

IL_002f:
	{
		Vector3_t2243707580  L_4 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_RotationDirection_3(L_4);
	}

IL_003a:
	{
		return;
	}
}
// System.Void SpinningCube::Update()
extern "C"  void SpinningCube_Update_m4275031041 (SpinningCube_t4269489377 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = __this->get_m_RotationDirection_3();
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_m_Speed_2();
		Vector3_t2243707580  L_5 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Rotate_m1743927093(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
