﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ButtonPresses
struct ButtonPresses_t321815673;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GPSloc
struct  GPSloc_t2883615338  : public MonoBehaviour_t1158329972
{
public:
	// System.Single GPSloc::latInit
	float ___latInit_2;
	// System.Single GPSloc::latNew
	float ___latNew_3;
	// System.Single GPSloc::longInit
	float ___longInit_4;
	// System.Single GPSloc::longNew
	float ___longNew_5;
	// System.Single GPSloc::deltaLat
	float ___deltaLat_6;
	// System.Single GPSloc::deltaLong
	float ___deltaLong_7;
	// System.Double GPSloc::transLat
	double ___transLat_8;
	// System.Double GPSloc::transLong
	double ___transLong_9;
	// ButtonPresses GPSloc::userName
	ButtonPresses_t321815673 * ___userName_10;

public:
	inline static int32_t get_offset_of_latInit_2() { return static_cast<int32_t>(offsetof(GPSloc_t2883615338, ___latInit_2)); }
	inline float get_latInit_2() const { return ___latInit_2; }
	inline float* get_address_of_latInit_2() { return &___latInit_2; }
	inline void set_latInit_2(float value)
	{
		___latInit_2 = value;
	}

	inline static int32_t get_offset_of_latNew_3() { return static_cast<int32_t>(offsetof(GPSloc_t2883615338, ___latNew_3)); }
	inline float get_latNew_3() const { return ___latNew_3; }
	inline float* get_address_of_latNew_3() { return &___latNew_3; }
	inline void set_latNew_3(float value)
	{
		___latNew_3 = value;
	}

	inline static int32_t get_offset_of_longInit_4() { return static_cast<int32_t>(offsetof(GPSloc_t2883615338, ___longInit_4)); }
	inline float get_longInit_4() const { return ___longInit_4; }
	inline float* get_address_of_longInit_4() { return &___longInit_4; }
	inline void set_longInit_4(float value)
	{
		___longInit_4 = value;
	}

	inline static int32_t get_offset_of_longNew_5() { return static_cast<int32_t>(offsetof(GPSloc_t2883615338, ___longNew_5)); }
	inline float get_longNew_5() const { return ___longNew_5; }
	inline float* get_address_of_longNew_5() { return &___longNew_5; }
	inline void set_longNew_5(float value)
	{
		___longNew_5 = value;
	}

	inline static int32_t get_offset_of_deltaLat_6() { return static_cast<int32_t>(offsetof(GPSloc_t2883615338, ___deltaLat_6)); }
	inline float get_deltaLat_6() const { return ___deltaLat_6; }
	inline float* get_address_of_deltaLat_6() { return &___deltaLat_6; }
	inline void set_deltaLat_6(float value)
	{
		___deltaLat_6 = value;
	}

	inline static int32_t get_offset_of_deltaLong_7() { return static_cast<int32_t>(offsetof(GPSloc_t2883615338, ___deltaLong_7)); }
	inline float get_deltaLong_7() const { return ___deltaLong_7; }
	inline float* get_address_of_deltaLong_7() { return &___deltaLong_7; }
	inline void set_deltaLong_7(float value)
	{
		___deltaLong_7 = value;
	}

	inline static int32_t get_offset_of_transLat_8() { return static_cast<int32_t>(offsetof(GPSloc_t2883615338, ___transLat_8)); }
	inline double get_transLat_8() const { return ___transLat_8; }
	inline double* get_address_of_transLat_8() { return &___transLat_8; }
	inline void set_transLat_8(double value)
	{
		___transLat_8 = value;
	}

	inline static int32_t get_offset_of_transLong_9() { return static_cast<int32_t>(offsetof(GPSloc_t2883615338, ___transLong_9)); }
	inline double get_transLong_9() const { return ___transLong_9; }
	inline double* get_address_of_transLong_9() { return &___transLong_9; }
	inline void set_transLong_9(double value)
	{
		___transLong_9 = value;
	}

	inline static int32_t get_offset_of_userName_10() { return static_cast<int32_t>(offsetof(GPSloc_t2883615338, ___userName_10)); }
	inline ButtonPresses_t321815673 * get_userName_10() const { return ___userName_10; }
	inline ButtonPresses_t321815673 ** get_address_of_userName_10() { return &___userName_10; }
	inline void set_userName_10(ButtonPresses_t321815673 * value)
	{
		___userName_10 = value;
		Il2CppCodeGenWriteBarrier(&___userName_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
