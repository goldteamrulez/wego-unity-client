﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPSloc/<GetLoc>c__Iterator1
struct U3CGetLocU3Ec__Iterator1_t539642169;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GPSloc/<GetLoc>c__Iterator1::.ctor()
extern "C"  void U3CGetLocU3Ec__Iterator1__ctor_m1765631764 (U3CGetLocU3Ec__Iterator1_t539642169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GPSloc/<GetLoc>c__Iterator1::MoveNext()
extern "C"  bool U3CGetLocU3Ec__Iterator1_MoveNext_m245081532 (U3CGetLocU3Ec__Iterator1_t539642169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPSloc/<GetLoc>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetLocU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m299087782 (U3CGetLocU3Ec__Iterator1_t539642169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPSloc/<GetLoc>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetLocU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3982940942 (U3CGetLocU3Ec__Iterator1_t539642169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPSloc/<GetLoc>c__Iterator1::Dispose()
extern "C"  void U3CGetLocU3Ec__Iterator1_Dispose_m2167591149 (U3CGetLocU3Ec__Iterator1_t539642169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPSloc/<GetLoc>c__Iterator1::Reset()
extern "C"  void U3CGetLocU3Ec__Iterator1_Reset_m673129679 (U3CGetLocU3Ec__Iterator1_t539642169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
