﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPSloc
struct GPSloc_t2883615338;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void GPSloc::.ctor()
extern "C"  void GPSloc__ctor_m3040463649 (GPSloc_t2883615338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GPSloc::Start()
extern "C"  Il2CppObject * GPSloc_Start_m2385954787 (GPSloc_t2883615338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPSloc::Update()
extern "C"  void GPSloc_Update_m556362346 (GPSloc_t2883615338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GPSloc::GetLoc()
extern "C"  Il2CppObject * GPSloc_GetLoc_m3042327473 (GPSloc_t2883615338 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
