﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.String
struct String_t;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HTTPreq/<Upload>c__Iterator1
struct  U3CUploadU3Ec__Iterator1_t4229958192  : public Il2CppObject
{
public:
	// UnityEngine.WWWForm HTTPreq/<Upload>c__Iterator1::<form>__0
	WWWForm_t3950226929 * ___U3CformU3E__0_0;
	// System.String HTTPreq/<Upload>c__Iterator1::name
	String_t* ___name_1;
	// System.String HTTPreq/<Upload>c__Iterator1::lat
	String_t* ___lat_2;
	// System.String HTTPreq/<Upload>c__Iterator1::lon
	String_t* ___lon_3;
	// System.String HTTPreq/<Upload>c__Iterator1::extension
	String_t* ___extension_4;
	// UnityEngine.Networking.UnityWebRequest HTTPreq/<Upload>c__Iterator1::<www>__1
	UnityWebRequest_t254341728 * ___U3CwwwU3E__1_5;
	// System.Object HTTPreq/<Upload>c__Iterator1::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean HTTPreq/<Upload>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 HTTPreq/<Upload>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUploadU3Ec__Iterator1_t4229958192, ___U3CformU3E__0_0)); }
	inline WWWForm_t3950226929 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t3950226929 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t3950226929 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__0_0, value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(U3CUploadU3Ec__Iterator1_t4229958192, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_lat_2() { return static_cast<int32_t>(offsetof(U3CUploadU3Ec__Iterator1_t4229958192, ___lat_2)); }
	inline String_t* get_lat_2() const { return ___lat_2; }
	inline String_t** get_address_of_lat_2() { return &___lat_2; }
	inline void set_lat_2(String_t* value)
	{
		___lat_2 = value;
		Il2CppCodeGenWriteBarrier(&___lat_2, value);
	}

	inline static int32_t get_offset_of_lon_3() { return static_cast<int32_t>(offsetof(U3CUploadU3Ec__Iterator1_t4229958192, ___lon_3)); }
	inline String_t* get_lon_3() const { return ___lon_3; }
	inline String_t** get_address_of_lon_3() { return &___lon_3; }
	inline void set_lon_3(String_t* value)
	{
		___lon_3 = value;
		Il2CppCodeGenWriteBarrier(&___lon_3, value);
	}

	inline static int32_t get_offset_of_extension_4() { return static_cast<int32_t>(offsetof(U3CUploadU3Ec__Iterator1_t4229958192, ___extension_4)); }
	inline String_t* get_extension_4() const { return ___extension_4; }
	inline String_t** get_address_of_extension_4() { return &___extension_4; }
	inline void set_extension_4(String_t* value)
	{
		___extension_4 = value;
		Il2CppCodeGenWriteBarrier(&___extension_4, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_5() { return static_cast<int32_t>(offsetof(U3CUploadU3Ec__Iterator1_t4229958192, ___U3CwwwU3E__1_5)); }
	inline UnityWebRequest_t254341728 * get_U3CwwwU3E__1_5() const { return ___U3CwwwU3E__1_5; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CwwwU3E__1_5() { return &___U3CwwwU3E__1_5; }
	inline void set_U3CwwwU3E__1_5(UnityWebRequest_t254341728 * value)
	{
		___U3CwwwU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__1_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CUploadU3Ec__Iterator1_t4229958192, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CUploadU3Ec__Iterator1_t4229958192, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CUploadU3Ec__Iterator1_t4229958192, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
