﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UpdateUserName
struct UpdateUserName_t1355389473;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonPresses
struct  ButtonPresses_t321815673  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ButtonPresses::textName
	bool ___textName_2;
	// System.String ButtonPresses::newName
	String_t* ___newName_3;
	// UnityEngine.UI.Text ButtonPresses::text
	Text_t356221433 * ___text_4;
	// UnityEngine.UI.Text ButtonPresses::submittedText
	Text_t356221433 * ___submittedText_5;
	// UnityEngine.GameObject ButtonPresses::myObj
	GameObject_t1756533147 * ___myObj_6;
	// UpdateUserName ButtonPresses::placeHolder
	UpdateUserName_t1355389473 * ___placeHolder_7;

public:
	inline static int32_t get_offset_of_textName_2() { return static_cast<int32_t>(offsetof(ButtonPresses_t321815673, ___textName_2)); }
	inline bool get_textName_2() const { return ___textName_2; }
	inline bool* get_address_of_textName_2() { return &___textName_2; }
	inline void set_textName_2(bool value)
	{
		___textName_2 = value;
	}

	inline static int32_t get_offset_of_newName_3() { return static_cast<int32_t>(offsetof(ButtonPresses_t321815673, ___newName_3)); }
	inline String_t* get_newName_3() const { return ___newName_3; }
	inline String_t** get_address_of_newName_3() { return &___newName_3; }
	inline void set_newName_3(String_t* value)
	{
		___newName_3 = value;
		Il2CppCodeGenWriteBarrier(&___newName_3, value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(ButtonPresses_t321815673, ___text_4)); }
	inline Text_t356221433 * get_text_4() const { return ___text_4; }
	inline Text_t356221433 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_t356221433 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier(&___text_4, value);
	}

	inline static int32_t get_offset_of_submittedText_5() { return static_cast<int32_t>(offsetof(ButtonPresses_t321815673, ___submittedText_5)); }
	inline Text_t356221433 * get_submittedText_5() const { return ___submittedText_5; }
	inline Text_t356221433 ** get_address_of_submittedText_5() { return &___submittedText_5; }
	inline void set_submittedText_5(Text_t356221433 * value)
	{
		___submittedText_5 = value;
		Il2CppCodeGenWriteBarrier(&___submittedText_5, value);
	}

	inline static int32_t get_offset_of_myObj_6() { return static_cast<int32_t>(offsetof(ButtonPresses_t321815673, ___myObj_6)); }
	inline GameObject_t1756533147 * get_myObj_6() const { return ___myObj_6; }
	inline GameObject_t1756533147 ** get_address_of_myObj_6() { return &___myObj_6; }
	inline void set_myObj_6(GameObject_t1756533147 * value)
	{
		___myObj_6 = value;
		Il2CppCodeGenWriteBarrier(&___myObj_6, value);
	}

	inline static int32_t get_offset_of_placeHolder_7() { return static_cast<int32_t>(offsetof(ButtonPresses_t321815673, ___placeHolder_7)); }
	inline UpdateUserName_t1355389473 * get_placeHolder_7() const { return ___placeHolder_7; }
	inline UpdateUserName_t1355389473 ** get_address_of_placeHolder_7() { return &___placeHolder_7; }
	inline void set_placeHolder_7(UpdateUserName_t1355389473 * value)
	{
		___placeHolder_7 = value;
		Il2CppCodeGenWriteBarrier(&___placeHolder_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
