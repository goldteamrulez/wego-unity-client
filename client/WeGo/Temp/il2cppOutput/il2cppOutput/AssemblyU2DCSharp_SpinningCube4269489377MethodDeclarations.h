﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SpinningCube
struct SpinningCube_t4269489377;

#include "codegen/il2cpp-codegen.h"

// System.Void SpinningCube::.ctor()
extern "C"  void SpinningCube__ctor_m2246610738 (SpinningCube_t4269489377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpinningCube::ToggleRotationDirection()
extern "C"  void SpinningCube_ToggleRotationDirection_m2472367075 (SpinningCube_t4269489377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SpinningCube::Update()
extern "C"  void SpinningCube_Update_m4275031041 (SpinningCube_t4269489377 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
