﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GPSloc/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t989442324;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GPSloc/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m166647329 (U3CStartU3Ec__Iterator0_t989442324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GPSloc/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m1088330335 (U3CStartU3Ec__Iterator0_t989442324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPSloc/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1458012559 (U3CStartU3Ec__Iterator0_t989442324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GPSloc/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m280312135 (U3CStartU3Ec__Iterator0_t989442324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPSloc/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m3897017584 (U3CStartU3Ec__Iterator0_t989442324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GPSloc/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m1888615354 (U3CStartU3Ec__Iterator0_t989442324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
