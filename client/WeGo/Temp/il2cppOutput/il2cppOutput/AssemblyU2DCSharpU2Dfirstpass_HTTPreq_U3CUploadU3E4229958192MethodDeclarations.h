﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HTTPreq/<Upload>c__Iterator1
struct U3CUploadU3Ec__Iterator1_t4229958192;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HTTPreq/<Upload>c__Iterator1::.ctor()
extern "C"  void U3CUploadU3Ec__Iterator1__ctor_m4181616813 (U3CUploadU3Ec__Iterator1_t4229958192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HTTPreq/<Upload>c__Iterator1::MoveNext()
extern "C"  bool U3CUploadU3Ec__Iterator1_MoveNext_m1186392159 (U3CUploadU3Ec__Iterator1_t4229958192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HTTPreq/<Upload>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUploadU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3201512287 (U3CUploadU3Ec__Iterator1_t4229958192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HTTPreq/<Upload>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUploadU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2834599207 (U3CUploadU3Ec__Iterator1_t4229958192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HTTPreq/<Upload>c__Iterator1::Dispose()
extern "C"  void U3CUploadU3Ec__Iterator1_Dispose_m2159774800 (U3CUploadU3Ec__Iterator1_t4229958192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HTTPreq/<Upload>c__Iterator1::Reset()
extern "C"  void U3CUploadU3Ec__Iterator1_Reset_m2495954550 (U3CUploadU3Ec__Iterator1_t4229958192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
