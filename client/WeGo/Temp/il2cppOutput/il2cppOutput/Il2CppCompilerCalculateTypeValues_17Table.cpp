﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GPSloc_U3CGetLocU3Ec_539642169.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HTTPreq3372007544.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HTTPreq_U3CGetTextU34204791727.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HTTPreq_U3CUploadU3E4229958192.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1249311527.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3088401398.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UpdateUserName1355389473.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (U3CGetLocU3Ec__Iterator1_t539642169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[5] = 
{
	U3CGetLocU3Ec__Iterator1_t539642169::get_offset_of_U3CmaxWaitU3E__0_0(),
	U3CGetLocU3Ec__Iterator1_t539642169::get_offset_of_U24this_1(),
	U3CGetLocU3Ec__Iterator1_t539642169::get_offset_of_U24current_2(),
	U3CGetLocU3Ec__Iterator1_t539642169::get_offset_of_U24disposing_3(),
	U3CGetLocU3Ec__Iterator1_t539642169::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (HTTPreq_t3372007544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (U3CGetTextU3Ec__Iterator0_t4204791727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[4] = 
{
	U3CGetTextU3Ec__Iterator0_t4204791727::get_offset_of_U3CwwwU3E__0_0(),
	U3CGetTextU3Ec__Iterator0_t4204791727::get_offset_of_U24current_1(),
	U3CGetTextU3Ec__Iterator0_t4204791727::get_offset_of_U24disposing_2(),
	U3CGetTextU3Ec__Iterator0_t4204791727::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (U3CUploadU3Ec__Iterator1_t4229958192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[9] = 
{
	U3CUploadU3Ec__Iterator1_t4229958192::get_offset_of_U3CformU3E__0_0(),
	U3CUploadU3Ec__Iterator1_t4229958192::get_offset_of_name_1(),
	U3CUploadU3Ec__Iterator1_t4229958192::get_offset_of_lat_2(),
	U3CUploadU3Ec__Iterator1_t4229958192::get_offset_of_lon_3(),
	U3CUploadU3Ec__Iterator1_t4229958192::get_offset_of_extension_4(),
	U3CUploadU3Ec__Iterator1_t4229958192::get_offset_of_U3CwwwU3E__1_5(),
	U3CUploadU3Ec__Iterator1_t4229958192::get_offset_of_U24current_6(),
	U3CUploadU3Ec__Iterator1_t4229958192::get_offset_of_U24disposing_7(),
	U3CUploadU3Ec__Iterator1_t4229958192::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (ThirdPersonCharacter_t1249311527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[20] = 
{
	ThirdPersonCharacter_t1249311527::get_offset_of_m_MovingTurnSpeed_2(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_StationaryTurnSpeed_3(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_JumpPower_4(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_GravityMultiplier_5(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_RunCycleLegOffset_6(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_MoveSpeedMultiplier_7(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_AnimSpeedMultiplier_8(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_GroundCheckDistance_9(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_Rigidbody_10(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_Animator_11(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_IsGrounded_12(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_OrigGroundCheckDistance_13(),
	0,
	ThirdPersonCharacter_t1249311527::get_offset_of_m_TurnAmount_15(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_ForwardAmount_16(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_GroundNormal_17(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_CapsuleHeight_18(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_CapsuleCenter_19(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_Capsule_20(),
	ThirdPersonCharacter_t1249311527::get_offset_of_m_Crouching_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (ThirdPersonUserControl_t3088401398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[6] = 
{
	ThirdPersonUserControl_t3088401398::get_offset_of_m_Character_2(),
	ThirdPersonUserControl_t3088401398::get_offset_of_m_Cam_3(),
	ThirdPersonUserControl_t3088401398::get_offset_of_m_CamForward_4(),
	ThirdPersonUserControl_t3088401398::get_offset_of_m_Move_5(),
	ThirdPersonUserControl_t3088401398::get_offset_of_m_Jump_6(),
	ThirdPersonUserControl_t3088401398::get_offset_of_gpsloc_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (UpdateUserName_t1355389473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[2] = 
{
	UpdateUserName_t1355389473::get_offset_of_info_2(),
	UpdateUserName_t1355389473::get_offset_of_textField_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
