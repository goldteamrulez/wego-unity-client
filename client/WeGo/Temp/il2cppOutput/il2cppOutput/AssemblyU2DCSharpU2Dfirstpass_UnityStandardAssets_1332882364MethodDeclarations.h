﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Characters.ThirdPerson.AICharacterControl
struct AICharacterControl_t1332882364;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t2761625415;
// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct ThirdPersonCharacter_t1249311527;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AI_NavMeshAgent2761625415.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1249311527.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::.ctor()
extern "C"  void AICharacterControl__ctor_m1008266682 (AICharacterControl_t1332882364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AI.NavMeshAgent UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::get_agent()
extern "C"  NavMeshAgent_t2761625415 * AICharacterControl_get_agent_m3648899759 (AICharacterControl_t1332882364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::set_agent(UnityEngine.AI.NavMeshAgent)
extern "C"  void AICharacterControl_set_agent_m2116505926 (AICharacterControl_t1332882364 * __this, NavMeshAgent_t2761625415 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::get_character()
extern "C"  ThirdPersonCharacter_t1249311527 * AICharacterControl_get_character_m1830520339 (AICharacterControl_t1332882364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::set_character(UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter)
extern "C"  void AICharacterControl_set_character_m3877059032 (AICharacterControl_t1332882364 * __this, ThirdPersonCharacter_t1249311527 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::Start()
extern "C"  void AICharacterControl_Start_m360647038 (AICharacterControl_t1332882364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::Update()
extern "C"  void AICharacterControl_Update_m2978625831 (AICharacterControl_t1332882364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::SetTarget(UnityEngine.Transform)
extern "C"  void AICharacterControl_SetTarget_m2789065236 (AICharacterControl_t1332882364 * __this, Transform_t3275118058 * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
