﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateUserName
struct UpdateUserName_t1355389473;

#include "codegen/il2cpp-codegen.h"

// System.Void UpdateUserName::.ctor()
extern "C"  void UpdateUserName__ctor_m2201865142 (UpdateUserName_t1355389473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateUserName::Start()
extern "C"  void UpdateUserName_Start_m143587690 (UpdateUserName_t1355389473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateUserName::Update()
extern "C"  void UpdateUserName_Update_m4053309911 (UpdateUserName_t1355389473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
