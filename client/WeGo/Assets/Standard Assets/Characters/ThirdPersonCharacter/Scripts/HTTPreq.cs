﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class HTTPreq: MonoBehaviour {
	void Start() {
		//StartCoroutine(GetText());
//		StartCoroutine(Upload("name", "hello", "visit"));
//		StartCoroutine(Upload("name", "my", "visit"));
//		StartCoroutine(Upload("name", "name", "visit"));
//		StartCoroutine(Upload("name", "is", "visit"));
//		StartCoroutine(Upload("name", "Michaela", "visit"));
		//StartCoroutine(GetText());
	}

	IEnumerator GetText() {
		UnityWebRequest www = UnityWebRequest.Get("http://35.165.191.227:8080/");
		yield return www.Send();
		//print("here1");

		if(www.isError) {
			Debug.Log(www.error);
			//print("here2");
		}
		else {
			// Show results as text
			Debug.Log(www.downloadHandler.text);

			// Or retrieve results as binary data
			byte[] results = www.downloadHandler.data;
			//print("here3");
		}
	}

	public static IEnumerator Upload(string name, string lat, string lon, string extension) {
		WWWForm form = new WWWForm();
		//form.AddField("name", "Cutie");
		form.AddField("userId", name);
		form.AddField("lat", lat);
		form.AddField("lon", lon);

		UnityWebRequest www = UnityWebRequest.Post(string.Concat("http://35.165.191.227:8080/", extension), form);
		yield return www.Send();

		if(www.isError) {
			Debug.Log(www.error);
		}
		else {
			Debug.Log("Form upload complete!");
			Debug.Log(www.downloadHandler.text);

//			UnityWebRequest site = UnityWebRequest.Get("http://35.165.191.227:8080/");
//			yield return site.Send();
//			Debug.Log(site.downloadHandler.text);
		}
	}
}