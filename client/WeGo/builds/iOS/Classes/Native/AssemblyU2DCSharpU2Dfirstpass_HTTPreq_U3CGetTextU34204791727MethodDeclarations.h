﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HTTPreq/<GetText>c__Iterator0
struct U3CGetTextU3Ec__Iterator0_t4204791727;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HTTPreq/<GetText>c__Iterator0::.ctor()
extern "C"  void U3CGetTextU3Ec__Iterator0__ctor_m1476118560 (U3CGetTextU3Ec__Iterator0_t4204791727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HTTPreq/<GetText>c__Iterator0::MoveNext()
extern "C"  bool U3CGetTextU3Ec__Iterator0_MoveNext_m536150256 (U3CGetTextU3Ec__Iterator0_t4204791727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HTTPreq/<GetText>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetTextU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4130864590 (U3CGetTextU3Ec__Iterator0_t4204791727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HTTPreq/<GetText>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetTextU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4039910070 (U3CGetTextU3Ec__Iterator0_t4204791727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HTTPreq/<GetText>c__Iterator0::Dispose()
extern "C"  void U3CGetTextU3Ec__Iterator0_Dispose_m1748222303 (U3CGetTextU3Ec__Iterator0_t4204791727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HTTPreq/<GetText>c__Iterator0::Reset()
extern "C"  void U3CGetTextU3Ec__Iterator0_Reset_m3008596221 (U3CGetTextU3Ec__Iterator0_t4204791727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
