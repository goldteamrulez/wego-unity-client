﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonPresses
struct ButtonPresses_t321815673;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonPresses::.ctor()
extern "C"  void ButtonPresses__ctor_m2555580292 (ButtonPresses_t321815673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonPresses::Start()
extern "C"  void ButtonPresses_Start_m2974068600 (ButtonPresses_t321815673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonPresses::UpdateName()
extern "C"  void ButtonPresses_UpdateName_m658659396 (ButtonPresses_t321815673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
