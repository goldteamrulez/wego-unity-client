﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// ButtonPresses
struct ButtonPresses_t321815673;
// GPSloc
struct GPSloc_t2883615338;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// GPSloc/<GetLoc>c__Iterator1
struct U3CGetLocU3Ec__Iterator1_t539642169;
// System.Object
struct Il2CppObject;
// GPSloc/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t989442324;
// HTTPreq
struct HTTPreq_t3372007544;
// System.String
struct String_t;
// HTTPreq/<GetText>c__Iterator0
struct U3CGetTextU3Ec__Iterator0_t4204791727;
// HTTPreq/<Upload>c__Iterator1
struct U3CUploadU3Ec__Iterator1_t4229958192;
// UnityStandardAssets.Characters.ThirdPerson.AICharacterControl
struct AICharacterControl_t1332882364;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t2761625415;
// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct ThirdPersonCharacter_t1249311527;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t720607407;
// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl
struct ThirdPersonUserControl_t3088401398;
// UpdateUserName
struct UpdateUserName_t1355389473;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ButtonPresses321815673.h"
#include "AssemblyU2DCSharpU2Dfirstpass_ButtonPresses321815673MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UpdateUserName1355389473.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GPSloc2883615338.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GPSloc2883615338MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GPSloc_U3CStartU3Ec__989442324MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GPSloc_U3CStartU3Ec__989442324.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GPSloc_U3CGetLocU3Ec_539642169MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_GPSloc_U3CGetLocU3Ec_539642169.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LocationService1617852714MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HTTPreq3372007544MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_LocationInfo1364725149.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_LocationService1617852714.h"
#include "UnityEngine_UnityEngine_LocationServiceStatus2482073234.h"
#include "UnityEngine_UnityEngine_LocationInfo1364725149MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HTTPreq3372007544.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HTTPreq_U3CGetTextU34204791727MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HTTPreq_U3CGetTextU34204791727.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HTTPreq_U3CUploadU3E4229958192MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HTTPreq_U3CUploadU3E4229958192.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest254341728MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1216180266MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest254341728.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1216180266.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1332882364.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1332882364MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AI_NavMeshAgent2761625415.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1249311527.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AI_NavMeshAgent2761625415MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_1249311527MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_CapsuleCollider720607407MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_CapsuleCollider720607407.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints251614631.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics634932869MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_Ray2469606224MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3088401398.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_3088401398MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UpdateUserName1355389473MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m2461586036(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.AI.NavMeshAgent>()
#define Component_GetComponentInChildren_TisNavMeshAgent_t2761625415_m2131776207(__this, method) ((  NavMeshAgent_t2761625415 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>()
#define Component_GetComponent_TisThirdPersonCharacter_t1249311527_m1051986587(__this, method) ((  ThirdPersonCharacter_t1249311527 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t69676727_m475627522(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t4233889191_m2323824819(__this, method) ((  Rigidbody_t4233889191 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CapsuleCollider>()
#define Component_GetComponent_TisCapsuleCollider_t720607407_m4236574705(__this, method) ((  CapsuleCollider_t720607407 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ButtonPresses::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ButtonPresses__ctor_m2555580292_MetadataUsageId;
extern "C"  void ButtonPresses__ctor_m2555580292 (ButtonPresses_t321815673 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonPresses__ctor_m2555580292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_newName_3(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonPresses::Start()
extern Il2CppCodeGenString* _stringLiteral3538157094;
extern const uint32_t ButtonPresses_Start_m2974068600_MetadataUsageId;
extern "C"  void ButtonPresses_Start_m2974068600 (ButtonPresses_t321815673 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonPresses_Start_m2974068600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_submittedText_5();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, _stringLiteral3538157094);
		return;
	}
}
// System.Void ButtonPresses::UpdateName()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2014250346;
extern Il2CppCodeGenString* _stringLiteral2551406963;
extern Il2CppCodeGenString* _stringLiteral3538157094;
extern Il2CppCodeGenString* _stringLiteral448243503;
extern const uint32_t ButtonPresses_UpdateName_m658659396_MetadataUsageId;
extern "C"  void ButtonPresses_UpdateName_m658659396 (ButtonPresses_t321815673 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonPresses_UpdateName_m658659396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_textName_2();
		__this->set_textName_2((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		bool L_1 = __this->get_textName_2();
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		Text_t356221433 * L_2 = __this->get_text_4();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, _stringLiteral2014250346);
		GameObject_t1756533147 * L_3 = __this->get_myObj_6();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		goto IL_0090;
	}

IL_003b:
	{
		UpdateUserName_t1355389473 * L_4 = __this->get_placeHolder_7();
		NullCheck(L_4);
		Text_t356221433 * L_5 = L_4->get_textField_3();
		Text_t356221433 * L_6 = __this->get_submittedText_5();
		NullCheck(L_6);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_6);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_7);
		Text_t356221433 * L_8 = __this->get_submittedText_5();
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2551406963, L_9, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Text_t356221433 * L_11 = __this->get_submittedText_5();
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, _stringLiteral3538157094);
		Text_t356221433 * L_12 = __this->get_text_4();
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, _stringLiteral448243503);
	}

IL_0090:
	{
		return;
	}
}
// System.Void GPSloc::.ctor()
extern "C"  void GPSloc__ctor_m3040463649 (GPSloc_t2883615338 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator GPSloc::Start()
extern Il2CppClass* U3CStartU3Ec__Iterator0_t989442324_il2cpp_TypeInfo_var;
extern const uint32_t GPSloc_Start_m2385954787_MetadataUsageId;
extern "C"  Il2CppObject * GPSloc_Start_m2385954787 (GPSloc_t2883615338 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GPSloc_Start_m2385954787_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStartU3Ec__Iterator0_t989442324 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t989442324 * L_0 = (U3CStartU3Ec__Iterator0_t989442324 *)il2cpp_codegen_object_new(U3CStartU3Ec__Iterator0_t989442324_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator0__ctor_m166647329(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator0_t989442324 * L_1 = V_0;
		return L_1;
	}
}
// System.Void GPSloc::Update()
extern "C"  void GPSloc_Update_m556362346 (GPSloc_t2883615338 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = GPSloc_GetLoc_m3042327473(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator GPSloc::GetLoc()
extern Il2CppClass* U3CGetLocU3Ec__Iterator1_t539642169_il2cpp_TypeInfo_var;
extern const uint32_t GPSloc_GetLoc_m3042327473_MetadataUsageId;
extern "C"  Il2CppObject * GPSloc_GetLoc_m3042327473 (GPSloc_t2883615338 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GPSloc_GetLoc_m3042327473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetLocU3Ec__Iterator1_t539642169 * V_0 = NULL;
	{
		U3CGetLocU3Ec__Iterator1_t539642169 * L_0 = (U3CGetLocU3Ec__Iterator1_t539642169 *)il2cpp_codegen_object_new(U3CGetLocU3Ec__Iterator1_t539642169_il2cpp_TypeInfo_var);
		U3CGetLocU3Ec__Iterator1__ctor_m1765631764(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetLocU3Ec__Iterator1_t539642169 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CGetLocU3Ec__Iterator1_t539642169 * L_2 = V_0;
		return L_2;
	}
}
// System.Void GPSloc/<GetLoc>c__Iterator1::.ctor()
extern "C"  void U3CGetLocU3Ec__Iterator1__ctor_m1765631764 (U3CGetLocU3Ec__Iterator1_t539642169 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GPSloc/<GetLoc>c__Iterator1::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2721984957;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern Il2CppCodeGenString* _stringLiteral2122523926;
extern Il2CppCodeGenString* _stringLiteral2958550092;
extern const uint32_t U3CGetLocU3Ec__Iterator1_MoveNext_m245081532_MetadataUsageId;
extern "C"  bool U3CGetLocU3Ec__Iterator1_MoveNext_m245081532 (U3CGetLocU3Ec__Iterator1_t539642169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetLocU3Ec__Iterator1_MoveNext_m245081532_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	LocationInfo_t1364725149  V_1;
	memset(&V_1, 0, sizeof(V_1));
	LocationInfo_t1364725149  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0052;
		}
	}
	{
		goto IL_02ac;
	}

IL_0021:
	{
		__this->set_U3CmaxWaitU3E__0_0(((int32_t)20));
		goto IL_0060;
	}

IL_002e:
	{
		WaitForSeconds_t3839502067 * L_2 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (1.0f), /*hidden argument*/NULL);
		__this->set_U24current_2(L_2);
		bool L_3 = __this->get_U24disposing_3();
		if (L_3)
		{
			goto IL_004d;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_004d:
	{
		goto IL_02ae;
	}

IL_0052:
	{
		int32_t L_4 = __this->get_U3CmaxWaitU3E__0_0();
		__this->set_U3CmaxWaitU3E__0_0(((int32_t)((int32_t)L_4-(int32_t)1)));
	}

IL_0060:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_5 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = LocationService_get_status_m1865246926(L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_7 = __this->get_U3CmaxWaitU3E__0_0();
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_002e;
		}
	}

IL_007c:
	{
		int32_t L_8 = __this->get_U3CmaxWaitU3E__0_0();
		if ((((int32_t)L_8) >= ((int32_t)1)))
		{
			goto IL_008d;
		}
	}
	{
		goto IL_02ac;
	}

IL_008d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_9 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = LocationService_get_status_m1865246926(L_9, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)3))))
		{
			goto IL_00a2;
		}
	}
	{
		goto IL_02ac;
	}

IL_00a2:
	{
		GPSloc_t2883615338 * L_11 = __this->get_U24this_1();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_12 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		LocationInfo_t1364725149  L_13 = LocationService_get_lastData_m2521124837(L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		float L_14 = LocationInfo_get_latitude_m2482205269((&V_1), /*hidden argument*/NULL);
		NullCheck(L_11);
		L_11->set_latNew_3(L_14);
		GPSloc_t2883615338 * L_15 = __this->get_U24this_1();
		LocationService_t1617852714 * L_16 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		LocationInfo_t1364725149  L_17 = LocationService_get_lastData_m2521124837(L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		float L_18 = LocationInfo_get_longitude_m306881672((&V_2), /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->set_longNew_5(L_18);
		GPSloc_t2883615338 * L_19 = __this->get_U24this_1();
		GPSloc_t2883615338 * L_20 = __this->get_U24this_1();
		NullCheck(L_20);
		float L_21 = L_20->get_latNew_3();
		GPSloc_t2883615338 * L_22 = __this->get_U24this_1();
		NullCheck(L_22);
		float L_23 = L_22->get_latInit_2();
		NullCheck(L_19);
		L_19->set_deltaLat_6(((float)((float)L_21-(float)L_23)));
		GPSloc_t2883615338 * L_24 = __this->get_U24this_1();
		GPSloc_t2883615338 * L_25 = __this->get_U24this_1();
		NullCheck(L_25);
		float L_26 = L_25->get_longNew_5();
		GPSloc_t2883615338 * L_27 = __this->get_U24this_1();
		NullCheck(L_27);
		float L_28 = L_27->get_longInit_4();
		NullCheck(L_24);
		L_24->set_deltaLong_7(((float)((float)L_26-(float)L_28)));
		GPSloc_t2883615338 * L_29 = __this->get_U24this_1();
		GPSloc_t2883615338 * L_30 = __this->get_U24this_1();
		NullCheck(L_30);
		float L_31 = L_30->get_deltaLat_6();
		NullCheck(L_29);
		L_29->set_transLat_8(((double)((double)(2341.9203747072597)*(double)(((double)((double)L_31))))));
		GPSloc_t2883615338 * L_32 = __this->get_U24this_1();
		GPSloc_t2883615338 * L_33 = __this->get_U24this_1();
		NullCheck(L_33);
		float L_34 = L_33->get_deltaLong_7();
		NullCheck(L_32);
		L_32->set_transLong_9(((double)((double)(1818.1818181818182)*(double)(((double)((double)L_34))))));
		ObjectU5BU5D_t3614634134* L_35 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, _stringLiteral2721984957);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2721984957);
		ObjectU5BU5D_t3614634134* L_36 = L_35;
		GPSloc_t2883615338 * L_37 = __this->get_U24this_1();
		NullCheck(L_37);
		float L_38 = L_37->get_deltaLat_6();
		float L_39 = L_38;
		Il2CppObject * L_40 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_40);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_40);
		ObjectU5BU5D_t3614634134* L_41 = L_36;
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, _stringLiteral372029310);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029310);
		ObjectU5BU5D_t3614634134* L_42 = L_41;
		GPSloc_t2883615338 * L_43 = __this->get_U24this_1();
		NullCheck(L_43);
		float L_44 = L_43->get_deltaLong_7();
		float L_45 = L_44;
		Il2CppObject * L_46 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_45);
		NullCheck(L_42);
		ArrayElementTypeCheck (L_42, L_46);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_46);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = String_Concat_m3881798623(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_48 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, _stringLiteral2122523926);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2122523926);
		ObjectU5BU5D_t3614634134* L_49 = L_48;
		GPSloc_t2883615338 * L_50 = __this->get_U24this_1();
		NullCheck(L_50);
		double L_51 = L_50->get_transLat_8();
		double L_52 = L_51;
		Il2CppObject * L_53 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_52);
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, L_53);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_53);
		ObjectU5BU5D_t3614634134* L_54 = L_49;
		NullCheck(L_54);
		ArrayElementTypeCheck (L_54, _stringLiteral372029310);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029310);
		ObjectU5BU5D_t3614634134* L_55 = L_54;
		GPSloc_t2883615338 * L_56 = __this->get_U24this_1();
		NullCheck(L_56);
		double L_57 = L_56->get_transLong_9();
		double L_58 = L_57;
		Il2CppObject * L_59 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_58);
		NullCheck(L_55);
		ArrayElementTypeCheck (L_55, L_59);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_59);
		String_t* L_60 = String_Concat_m3881798623(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		GPSloc_t2883615338 * L_61 = __this->get_U24this_1();
		NullCheck(L_61);
		float L_62 = L_61->get_deltaLat_6();
		if ((((double)(((double)((double)L_62)))) > ((double)(1.0E-05))))
		{
			goto IL_0222;
		}
	}
	{
		GPSloc_t2883615338 * L_63 = __this->get_U24this_1();
		NullCheck(L_63);
		float L_64 = L_63->get_deltaLong_7();
		if ((!(((double)(((double)((double)L_64)))) > ((double)(1.0E-05)))))
		{
			goto IL_0279;
		}
	}

IL_0222:
	{
		GPSloc_t2883615338 * L_65 = __this->get_U24this_1();
		GPSloc_t2883615338 * L_66 = __this->get_U24this_1();
		NullCheck(L_66);
		ButtonPresses_t321815673 * L_67 = L_66->get_userName_10();
		NullCheck(L_67);
		Text_t356221433 * L_68 = L_67->get_submittedText_5();
		NullCheck(L_68);
		String_t* L_69 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_68);
		GPSloc_t2883615338 * L_70 = __this->get_U24this_1();
		NullCheck(L_70);
		float* L_71 = L_70->get_address_of_latNew_3();
		String_t* L_72 = Single_ToString_m1813392066(L_71, /*hidden argument*/NULL);
		GPSloc_t2883615338 * L_73 = __this->get_U24this_1();
		NullCheck(L_73);
		float* L_74 = L_73->get_address_of_longNew_5();
		String_t* L_75 = Single_ToString_m1813392066(L_74, /*hidden argument*/NULL);
		Il2CppObject * L_76 = HTTPreq_Upload_m3931106518(NULL /*static, unused*/, L_69, L_72, L_75, _stringLiteral2958550092, /*hidden argument*/NULL);
		NullCheck(L_65);
		MonoBehaviour_StartCoroutine_m2470621050(L_65, L_76, /*hidden argument*/NULL);
	}

IL_0279:
	{
		GPSloc_t2883615338 * L_77 = __this->get_U24this_1();
		GPSloc_t2883615338 * L_78 = __this->get_U24this_1();
		NullCheck(L_78);
		float L_79 = L_78->get_latNew_3();
		NullCheck(L_77);
		L_77->set_latInit_2(L_79);
		GPSloc_t2883615338 * L_80 = __this->get_U24this_1();
		GPSloc_t2883615338 * L_81 = __this->get_U24this_1();
		NullCheck(L_81);
		float L_82 = L_81->get_longNew_5();
		NullCheck(L_80);
		L_80->set_longInit_4(L_82);
		__this->set_U24PC_4((-1));
	}

IL_02ac:
	{
		return (bool)0;
	}

IL_02ae:
	{
		return (bool)1;
	}
}
// System.Object GPSloc/<GetLoc>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetLocU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m299087782 (U3CGetLocU3Ec__Iterator1_t539642169 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object GPSloc/<GetLoc>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetLocU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3982940942 (U3CGetLocU3Ec__Iterator1_t539642169 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void GPSloc/<GetLoc>c__Iterator1::Dispose()
extern "C"  void U3CGetLocU3Ec__Iterator1_Dispose_m2167591149 (U3CGetLocU3Ec__Iterator1_t539642169 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void GPSloc/<GetLoc>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetLocU3Ec__Iterator1_Reset_m673129679_MetadataUsageId;
extern "C"  void U3CGetLocU3Ec__Iterator1_Reset_m673129679 (U3CGetLocU3Ec__Iterator1_t539642169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetLocU3Ec__Iterator1_Reset_m673129679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void GPSloc/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m166647329 (U3CStartU3Ec__Iterator0_t989442324 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GPSloc/<Start>c__Iterator0::MoveNext()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m1088330335_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m1088330335 (U3CStartU3Ec__Iterator0_t989442324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m1088330335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_U24PC_2();
		__this->set_U24PC_2((-1));
		if (L_0)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_1 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = LocationService_get_isEnabledByUser_m840009485(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0026;
		}
	}
	{
		goto IL_0030;
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		LocationService_t1617852714 * L_3 = Input_get_location_m1390884443(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		LocationService_Start_m3920984473(L_3, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return (bool)0;
	}
}
// System.Object GPSloc/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1458012559 (U3CStartU3Ec__Iterator0_t989442324 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Object GPSloc/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m280312135 (U3CStartU3Ec__Iterator0_t989442324 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_0();
		return L_0;
	}
}
// System.Void GPSloc/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m3897017584 (U3CStartU3Ec__Iterator0_t989442324 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GPSloc/<Start>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m1888615354_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m1888615354 (U3CStartU3Ec__Iterator0_t989442324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m1888615354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void HTTPreq::.ctor()
extern "C"  void HTTPreq__ctor_m1329710387 (HTTPreq_t3372007544 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HTTPreq::Start()
extern "C"  void HTTPreq_Start_m3797058471 (HTTPreq_t3372007544 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Collections.IEnumerator HTTPreq::GetText()
extern Il2CppClass* U3CGetTextU3Ec__Iterator0_t4204791727_il2cpp_TypeInfo_var;
extern const uint32_t HTTPreq_GetText_m1041910378_MetadataUsageId;
extern "C"  Il2CppObject * HTTPreq_GetText_m1041910378 (HTTPreq_t3372007544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HTTPreq_GetText_m1041910378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetTextU3Ec__Iterator0_t4204791727 * V_0 = NULL;
	{
		U3CGetTextU3Ec__Iterator0_t4204791727 * L_0 = (U3CGetTextU3Ec__Iterator0_t4204791727 *)il2cpp_codegen_object_new(U3CGetTextU3Ec__Iterator0_t4204791727_il2cpp_TypeInfo_var);
		U3CGetTextU3Ec__Iterator0__ctor_m1476118560(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetTextU3Ec__Iterator0_t4204791727 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator HTTPreq::Upload(System.String,System.String,System.String,System.String)
extern Il2CppClass* U3CUploadU3Ec__Iterator1_t4229958192_il2cpp_TypeInfo_var;
extern const uint32_t HTTPreq_Upload_m3931106518_MetadataUsageId;
extern "C"  Il2CppObject * HTTPreq_Upload_m3931106518 (Il2CppObject * __this /* static, unused */, String_t* ___name0, String_t* ___lat1, String_t* ___lon2, String_t* ___extension3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HTTPreq_Upload_m3931106518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CUploadU3Ec__Iterator1_t4229958192 * V_0 = NULL;
	{
		U3CUploadU3Ec__Iterator1_t4229958192 * L_0 = (U3CUploadU3Ec__Iterator1_t4229958192 *)il2cpp_codegen_object_new(U3CUploadU3Ec__Iterator1_t4229958192_il2cpp_TypeInfo_var);
		U3CUploadU3Ec__Iterator1__ctor_m4181616813(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CUploadU3Ec__Iterator1_t4229958192 * L_1 = V_0;
		String_t* L_2 = ___name0;
		NullCheck(L_1);
		L_1->set_name_1(L_2);
		U3CUploadU3Ec__Iterator1_t4229958192 * L_3 = V_0;
		String_t* L_4 = ___lat1;
		NullCheck(L_3);
		L_3->set_lat_2(L_4);
		U3CUploadU3Ec__Iterator1_t4229958192 * L_5 = V_0;
		String_t* L_6 = ___lon2;
		NullCheck(L_5);
		L_5->set_lon_3(L_6);
		U3CUploadU3Ec__Iterator1_t4229958192 * L_7 = V_0;
		String_t* L_8 = ___extension3;
		NullCheck(L_7);
		L_7->set_extension_4(L_8);
		U3CUploadU3Ec__Iterator1_t4229958192 * L_9 = V_0;
		return L_9;
	}
}
// System.Void HTTPreq/<GetText>c__Iterator0::.ctor()
extern "C"  void U3CGetTextU3Ec__Iterator0__ctor_m1476118560 (U3CGetTextU3Ec__Iterator0_t4204791727 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean HTTPreq/<GetText>c__Iterator0::MoveNext()
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2449782267;
extern const uint32_t U3CGetTextU3Ec__Iterator0_MoveNext_m536150256_MetadataUsageId;
extern "C"  bool U3CGetTextU3Ec__Iterator0_MoveNext_m536150256 (U3CGetTextU3Ec__Iterator0_t4204791727 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetTextU3Ec__Iterator0_MoveNext_m536150256_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0056;
		}
	}
	{
		goto IL_00a8;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest_t254341728 * L_2 = UnityWebRequest_Get_m3762133581(NULL /*static, unused*/, _stringLiteral2449782267, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_0(L_2);
		UnityWebRequest_t254341728 * L_3 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_3);
		AsyncOperation_t3814632279 * L_4 = UnityWebRequest_Send_m4070133578(L_3, /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		bool L_5 = __this->get_U24disposing_2();
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0051:
	{
		goto IL_00aa;
	}

IL_0056:
	{
		UnityWebRequest_t254341728 * L_6 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_6);
		bool L_7 = UnityWebRequest_get_isError_m2487408374(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007b;
		}
	}
	{
		UnityWebRequest_t254341728 * L_8 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_8);
		String_t* L_9 = UnityWebRequest_get_error_m1819765147(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		goto IL_00a1;
	}

IL_007b:
	{
		UnityWebRequest_t254341728 * L_10 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_10);
		DownloadHandler_t1216180266 * L_11 = UnityWebRequest_get_downloadHandler_m2794451840(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = DownloadHandler_get_text_m1879851762(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_13 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_13);
		DownloadHandler_t1216180266 * L_14 = UnityWebRequest_get_downloadHandler_m2794451840(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		ByteU5BU5D_t3397334013* L_15 = DownloadHandler_get_data_m3053640740(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
	}

IL_00a1:
	{
		__this->set_U24PC_3((-1));
	}

IL_00a8:
	{
		return (bool)0;
	}

IL_00aa:
	{
		return (bool)1;
	}
}
// System.Object HTTPreq/<GetText>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetTextU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4130864590 (U3CGetTextU3Ec__Iterator0_t4204791727 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object HTTPreq/<GetText>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetTextU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4039910070 (U3CGetTextU3Ec__Iterator0_t4204791727 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void HTTPreq/<GetText>c__Iterator0::Dispose()
extern "C"  void U3CGetTextU3Ec__Iterator0_Dispose_m1748222303 (U3CGetTextU3Ec__Iterator0_t4204791727 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void HTTPreq/<GetText>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetTextU3Ec__Iterator0_Reset_m3008596221_MetadataUsageId;
extern "C"  void U3CGetTextU3Ec__Iterator0_Reset_m3008596221 (U3CGetTextU3Ec__Iterator0_t4204791727 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetTextU3Ec__Iterator0_Reset_m3008596221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void HTTPreq/<Upload>c__Iterator1::.ctor()
extern "C"  void U3CUploadU3Ec__Iterator1__ctor_m4181616813 (U3CUploadU3Ec__Iterator1_t4229958192 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean HTTPreq/<Upload>c__Iterator1::MoveNext()
extern Il2CppClass* WWWForm_t3950226929_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2844854044;
extern Il2CppCodeGenString* _stringLiteral696029425;
extern Il2CppCodeGenString* _stringLiteral1502598457;
extern Il2CppCodeGenString* _stringLiteral2449782267;
extern Il2CppCodeGenString* _stringLiteral173026749;
extern const uint32_t U3CUploadU3Ec__Iterator1_MoveNext_m1186392159_MetadataUsageId;
extern "C"  bool U3CUploadU3Ec__Iterator1_MoveNext_m1186392159 (U3CUploadU3Ec__Iterator1_t4229958192 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CUploadU3Ec__Iterator1_MoveNext_m1186392159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_8();
		V_0 = L_0;
		__this->set_U24PC_8((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00b4;
		}
	}
	{
		goto IL_00ff;
	}

IL_0021:
	{
		WWWForm_t3950226929 * L_2 = (WWWForm_t3950226929 *)il2cpp_codegen_object_new(WWWForm_t3950226929_il2cpp_TypeInfo_var);
		WWWForm__ctor_m2129424870(L_2, /*hidden argument*/NULL);
		__this->set_U3CformU3E__0_0(L_2);
		WWWForm_t3950226929 * L_3 = __this->get_U3CformU3E__0_0();
		String_t* L_4 = __this->get_name_1();
		NullCheck(L_3);
		WWWForm_AddField_m1334606983(L_3, _stringLiteral2844854044, L_4, /*hidden argument*/NULL);
		WWWForm_t3950226929 * L_5 = __this->get_U3CformU3E__0_0();
		String_t* L_6 = __this->get_lat_2();
		NullCheck(L_5);
		WWWForm_AddField_m1334606983(L_5, _stringLiteral696029425, L_6, /*hidden argument*/NULL);
		WWWForm_t3950226929 * L_7 = __this->get_U3CformU3E__0_0();
		String_t* L_8 = __this->get_lon_3();
		NullCheck(L_7);
		WWWForm_AddField_m1334606983(L_7, _stringLiteral1502598457, L_8, /*hidden argument*/NULL);
		String_t* L_9 = __this->get_extension_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2449782267, L_9, /*hidden argument*/NULL);
		WWWForm_t3950226929 * L_11 = __this->get_U3CformU3E__0_0();
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest_t254341728 * L_12 = UnityWebRequest_Post_m1002780585(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__1_5(L_12);
		UnityWebRequest_t254341728 * L_13 = __this->get_U3CwwwU3E__1_5();
		NullCheck(L_13);
		AsyncOperation_t3814632279 * L_14 = UnityWebRequest_Send_m4070133578(L_13, /*hidden argument*/NULL);
		__this->set_U24current_6(L_14);
		bool L_15 = __this->get_U24disposing_7();
		if (L_15)
		{
			goto IL_00af;
		}
	}
	{
		__this->set_U24PC_8(1);
	}

IL_00af:
	{
		goto IL_0101;
	}

IL_00b4:
	{
		UnityWebRequest_t254341728 * L_16 = __this->get_U3CwwwU3E__1_5();
		NullCheck(L_16);
		bool L_17 = UnityWebRequest_get_isError_m2487408374(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00d9;
		}
	}
	{
		UnityWebRequest_t254341728 * L_18 = __this->get_U3CwwwU3E__1_5();
		NullCheck(L_18);
		String_t* L_19 = UnityWebRequest_get_error_m1819765147(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		goto IL_00f8;
	}

IL_00d9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral173026749, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_20 = __this->get_U3CwwwU3E__1_5();
		NullCheck(L_20);
		DownloadHandler_t1216180266 * L_21 = UnityWebRequest_get_downloadHandler_m2794451840(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = DownloadHandler_get_text_m1879851762(L_21, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
	}

IL_00f8:
	{
		__this->set_U24PC_8((-1));
	}

IL_00ff:
	{
		return (bool)0;
	}

IL_0101:
	{
		return (bool)1;
	}
}
// System.Object HTTPreq/<Upload>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUploadU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3201512287 (U3CUploadU3Ec__Iterator1_t4229958192 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object HTTPreq/<Upload>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUploadU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2834599207 (U3CUploadU3Ec__Iterator1_t4229958192 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Void HTTPreq/<Upload>c__Iterator1::Dispose()
extern "C"  void U3CUploadU3Ec__Iterator1_Dispose_m2159774800 (U3CUploadU3Ec__Iterator1_t4229958192 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_7((bool)1);
		__this->set_U24PC_8((-1));
		return;
	}
}
// System.Void HTTPreq/<Upload>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CUploadU3Ec__Iterator1_Reset_m2495954550_MetadataUsageId;
extern "C"  void U3CUploadU3Ec__Iterator1_Reset_m2495954550 (U3CUploadU3Ec__Iterator1_t4229958192 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CUploadU3Ec__Iterator1_Reset_m2495954550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::.ctor()
extern "C"  void AICharacterControl__ctor_m1008266682 (AICharacterControl_t1332882364 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AI.NavMeshAgent UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::get_agent()
extern "C"  NavMeshAgent_t2761625415 * AICharacterControl_get_agent_m3648899759 (AICharacterControl_t1332882364 * __this, const MethodInfo* method)
{
	{
		NavMeshAgent_t2761625415 * L_0 = __this->get_U3CagentU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::set_agent(UnityEngine.AI.NavMeshAgent)
extern "C"  void AICharacterControl_set_agent_m2116505926 (AICharacterControl_t1332882364 * __this, NavMeshAgent_t2761625415 * ___value0, const MethodInfo* method)
{
	{
		NavMeshAgent_t2761625415 * L_0 = ___value0;
		__this->set_U3CagentU3Ek__BackingField_2(L_0);
		return;
	}
}
// UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::get_character()
extern "C"  ThirdPersonCharacter_t1249311527 * AICharacterControl_get_character_m1830520339 (AICharacterControl_t1332882364 * __this, const MethodInfo* method)
{
	{
		ThirdPersonCharacter_t1249311527 * L_0 = __this->get_U3CcharacterU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::set_character(UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter)
extern "C"  void AICharacterControl_set_character_m3877059032 (AICharacterControl_t1332882364 * __this, ThirdPersonCharacter_t1249311527 * ___value0, const MethodInfo* method)
{
	{
		ThirdPersonCharacter_t1249311527 * L_0 = ___value0;
		__this->set_U3CcharacterU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::Start()
extern const MethodInfo* Component_GetComponentInChildren_TisNavMeshAgent_t2761625415_m2131776207_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisThirdPersonCharacter_t1249311527_m1051986587_MethodInfo_var;
extern const uint32_t AICharacterControl_Start_m360647038_MetadataUsageId;
extern "C"  void AICharacterControl_Start_m360647038 (AICharacterControl_t1332882364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AICharacterControl_Start_m360647038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NavMeshAgent_t2761625415 * L_0 = Component_GetComponentInChildren_TisNavMeshAgent_t2761625415_m2131776207(__this, /*hidden argument*/Component_GetComponentInChildren_TisNavMeshAgent_t2761625415_m2131776207_MethodInfo_var);
		AICharacterControl_set_agent_m2116505926(__this, L_0, /*hidden argument*/NULL);
		ThirdPersonCharacter_t1249311527 * L_1 = Component_GetComponent_TisThirdPersonCharacter_t1249311527_m1051986587(__this, /*hidden argument*/Component_GetComponent_TisThirdPersonCharacter_t1249311527_m1051986587_MethodInfo_var);
		AICharacterControl_set_character_m3877059032(__this, L_1, /*hidden argument*/NULL);
		NavMeshAgent_t2761625415 * L_2 = AICharacterControl_get_agent_m3648899759(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		NavMeshAgent_set_updateRotation_m2981003907(L_2, (bool)0, /*hidden argument*/NULL);
		NavMeshAgent_t2761625415 * L_3 = AICharacterControl_get_agent_m3648899759(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		NavMeshAgent_set_updatePosition_m3466016722(L_3, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t AICharacterControl_Update_m2978625831_MetadataUsageId;
extern "C"  void AICharacterControl_Update_m2978625831 (AICharacterControl_t1332882364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AICharacterControl_Update_m2978625831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = __this->get_target_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		NavMeshAgent_t2761625415 * L_2 = AICharacterControl_get_agent_m3648899759(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = __this->get_target_4();
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		NavMeshAgent_SetDestination_m1354616139(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		NavMeshAgent_t2761625415 * L_5 = AICharacterControl_get_agent_m3648899759(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		float L_6 = NavMeshAgent_get_remainingDistance_m2699477664(L_5, /*hidden argument*/NULL);
		NavMeshAgent_t2761625415 * L_7 = AICharacterControl_get_agent_m3648899759(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		float L_8 = NavMeshAgent_get_stoppingDistance_m456040076(L_7, /*hidden argument*/NULL);
		if ((!(((float)L_6) > ((float)L_8))))
		{
			goto IL_0060;
		}
	}
	{
		ThirdPersonCharacter_t1249311527 * L_9 = AICharacterControl_get_character_m1830520339(__this, /*hidden argument*/NULL);
		NavMeshAgent_t2761625415 * L_10 = AICharacterControl_get_agent_m3648899759(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = NavMeshAgent_get_desiredVelocity_m331356702(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		ThirdPersonCharacter_Move_m1991112899(L_9, L_11, (bool)0, (bool)0, /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_0060:
	{
		ThirdPersonCharacter_t1249311527 * L_12 = AICharacterControl_get_character_m1830520339(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		ThirdPersonCharacter_Move_m1991112899(L_12, L_13, (bool)0, (bool)0, /*hidden argument*/NULL);
	}

IL_0072:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.AICharacterControl::SetTarget(UnityEngine.Transform)
extern "C"  void AICharacterControl_SetTarget_m2789065236 (AICharacterControl_t1332882364 * __this, Transform_t3275118058 * ___target0, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = ___target0;
		__this->set_target_4(L_0);
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::.ctor()
extern "C"  void ThirdPersonCharacter__ctor_m1081907647 (ThirdPersonCharacter_t1249311527 * __this, const MethodInfo* method)
{
	{
		__this->set_m_MovingTurnSpeed_2((360.0f));
		__this->set_m_StationaryTurnSpeed_3((180.0f));
		__this->set_m_JumpPower_4((12.0f));
		__this->set_m_GravityMultiplier_5((2.0f));
		__this->set_m_RunCycleLegOffset_6((0.2f));
		__this->set_m_MoveSpeedMultiplier_7((1.0f));
		__this->set_m_AnimSpeedMultiplier_8((1.0f));
		__this->set_m_GroundCheckDistance_9((0.1f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::Start()
extern const MethodInfo* Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t4233889191_m2323824819_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCapsuleCollider_t720607407_m4236574705_MethodInfo_var;
extern const uint32_t ThirdPersonCharacter_Start_m1843688155_MetadataUsageId;
extern "C"  void ThirdPersonCharacter_Start_m1843688155 (ThirdPersonCharacter_t1249311527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonCharacter_Start_m1843688155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animator_t69676727 * L_0 = Component_GetComponent_TisAnimator_t69676727_m475627522(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_MethodInfo_var);
		__this->set_m_Animator_11(L_0);
		Rigidbody_t4233889191 * L_1 = Component_GetComponent_TisRigidbody_t4233889191_m2323824819(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m2323824819_MethodInfo_var);
		__this->set_m_Rigidbody_10(L_1);
		CapsuleCollider_t720607407 * L_2 = Component_GetComponent_TisCapsuleCollider_t720607407_m4236574705(__this, /*hidden argument*/Component_GetComponent_TisCapsuleCollider_t720607407_m4236574705_MethodInfo_var);
		__this->set_m_Capsule_20(L_2);
		CapsuleCollider_t720607407 * L_3 = __this->get_m_Capsule_20();
		NullCheck(L_3);
		float L_4 = CapsuleCollider_get_height_m2987501502(L_3, /*hidden argument*/NULL);
		__this->set_m_CapsuleHeight_18(L_4);
		CapsuleCollider_t720607407 * L_5 = __this->get_m_Capsule_20();
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = CapsuleCollider_get_center_m189177564(L_5, /*hidden argument*/NULL);
		__this->set_m_CapsuleCenter_19(L_6);
		Rigidbody_t4233889191 * L_7 = __this->get_m_Rigidbody_10();
		NullCheck(L_7);
		Rigidbody_set_constraints_m3474016139(L_7, ((int32_t)112), /*hidden argument*/NULL);
		float L_8 = __this->get_m_GroundCheckDistance_9();
		__this->set_m_OrigGroundCheckDistance_13(L_8);
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::Move(UnityEngine.Vector3,System.Boolean,System.Boolean)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t ThirdPersonCharacter_Move_m1991112899_MetadataUsageId;
extern "C"  void ThirdPersonCharacter_Move_m1991112899 (ThirdPersonCharacter_t1249311527 * __this, Vector3_t2243707580  ___move0, bool ___crouch1, bool ___jump2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonCharacter_Move_m1991112899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Vector3_get_magnitude_m860342598((&___move0), /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)(1.0f)))))
		{
			goto IL_0018;
		}
	}
	{
		Vector3_Normalize_m3679112426((&___move0), /*hidden argument*/NULL);
	}

IL_0018:
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = ___move0;
		NullCheck(L_1);
		Vector3_t2243707580  L_3 = Transform_InverseTransformDirection_m3595190459(L_1, L_2, /*hidden argument*/NULL);
		___move0 = L_3;
		ThirdPersonCharacter_CheckGroundStatus_m2508098784(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = ___move0;
		Vector3_t2243707580  L_5 = __this->get_m_GroundNormal_17();
		Vector3_t2243707580  L_6 = Vector3_ProjectOnPlane_m4151370583(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		___move0 = L_6;
		float L_7 = (&___move0)->get_x_1();
		float L_8 = (&___move0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_9 = atan2f(L_7, L_8);
		__this->set_m_TurnAmount_15(L_9);
		float L_10 = (&___move0)->get_z_3();
		__this->set_m_ForwardAmount_16(L_10);
		ThirdPersonCharacter_ApplyExtraTurnRotation_m2992115070(__this, /*hidden argument*/NULL);
		bool L_11 = __this->get_m_IsGrounded_12();
		if (!L_11)
		{
			goto IL_007e;
		}
	}
	{
		bool L_12 = ___crouch1;
		bool L_13 = ___jump2;
		ThirdPersonCharacter_HandleGroundedMovement_m1707909628(__this, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0084;
	}

IL_007e:
	{
		ThirdPersonCharacter_HandleAirborneMovement_m4276350424(__this, /*hidden argument*/NULL);
	}

IL_0084:
	{
		bool L_14 = ___crouch1;
		ThirdPersonCharacter_ScaleCapsuleForCrouching_m2983744150(__this, L_14, /*hidden argument*/NULL);
		ThirdPersonCharacter_PreventStandingInLowHeadroom_m3349044725(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = ___move0;
		ThirdPersonCharacter_UpdateAnimator_m1348176094(__this, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::ScaleCapsuleForCrouching(System.Boolean)
extern "C"  void ThirdPersonCharacter_ScaleCapsuleForCrouching_m2983744150 (ThirdPersonCharacter_t1249311527 * __this, bool ___crouch0, const MethodInfo* method)
{
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		bool L_0 = __this->get_m_IsGrounded_12();
		if (!L_0)
		{
			goto IL_0065;
		}
	}
	{
		bool L_1 = ___crouch0;
		if (!L_1)
		{
			goto IL_0065;
		}
	}
	{
		bool L_2 = __this->get_m_Crouching_21();
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		return;
	}

IL_001d:
	{
		CapsuleCollider_t720607407 * L_3 = __this->get_m_Capsule_20();
		CapsuleCollider_t720607407 * L_4 = __this->get_m_Capsule_20();
		NullCheck(L_4);
		float L_5 = CapsuleCollider_get_height_m2987501502(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		CapsuleCollider_set_height_m643982877(L_3, ((float)((float)L_5/(float)(2.0f))), /*hidden argument*/NULL);
		CapsuleCollider_t720607407 * L_6 = __this->get_m_Capsule_20();
		CapsuleCollider_t720607407 * L_7 = __this->get_m_Capsule_20();
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = CapsuleCollider_get_center_m189177564(L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_8, (2.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		CapsuleCollider_set_center_m2483620541(L_6, L_9, /*hidden argument*/NULL);
		__this->set_m_Crouching_21((bool)1);
		goto IL_0109;
	}

IL_0065:
	{
		Rigidbody_t4233889191 * L_10 = __this->get_m_Rigidbody_10();
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Rigidbody_get_position_m3465583110(L_10, /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		CapsuleCollider_t720607407 * L_13 = __this->get_m_Capsule_20();
		NullCheck(L_13);
		float L_14 = CapsuleCollider_get_radius_m842698081(L_13, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_15, (0.5f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_17 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_11, L_16, /*hidden argument*/NULL);
		Vector3_t2243707580  L_18 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray__ctor_m3379034047((&V_0), L_17, L_18, /*hidden argument*/NULL);
		float L_19 = __this->get_m_CapsuleHeight_18();
		CapsuleCollider_t720607407 * L_20 = __this->get_m_Capsule_20();
		NullCheck(L_20);
		float L_21 = CapsuleCollider_get_radius_m842698081(L_20, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_19-(float)((float)((float)L_21*(float)(0.5f)))));
		Ray_t2469606224  L_22 = V_0;
		CapsuleCollider_t720607407 * L_23 = __this->get_m_Capsule_20();
		NullCheck(L_23);
		float L_24 = CapsuleCollider_get_radius_m842698081(L_23, /*hidden argument*/NULL);
		float L_25 = V_1;
		bool L_26 = Physics_SphereCast_m402220965(NULL /*static, unused*/, L_22, ((float)((float)L_24*(float)(0.5f))), L_25, (-1), 1, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00e0;
		}
	}
	{
		__this->set_m_Crouching_21((bool)1);
		return;
	}

IL_00e0:
	{
		CapsuleCollider_t720607407 * L_27 = __this->get_m_Capsule_20();
		float L_28 = __this->get_m_CapsuleHeight_18();
		NullCheck(L_27);
		CapsuleCollider_set_height_m643982877(L_27, L_28, /*hidden argument*/NULL);
		CapsuleCollider_t720607407 * L_29 = __this->get_m_Capsule_20();
		Vector3_t2243707580  L_30 = __this->get_m_CapsuleCenter_19();
		NullCheck(L_29);
		CapsuleCollider_set_center_m2483620541(L_29, L_30, /*hidden argument*/NULL);
		__this->set_m_Crouching_21((bool)0);
	}

IL_0109:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::PreventStandingInLowHeadroom()
extern "C"  void ThirdPersonCharacter_PreventStandingInLowHeadroom_m3349044725 (ThirdPersonCharacter_t1249311527 * __this, const MethodInfo* method)
{
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		bool L_0 = __this->get_m_Crouching_21();
		if (L_0)
		{
			goto IL_0085;
		}
	}
	{
		Rigidbody_t4233889191 * L_1 = __this->get_m_Rigidbody_10();
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Rigidbody_get_position_m3465583110(L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		CapsuleCollider_t720607407 * L_4 = __this->get_m_Capsule_20();
		NullCheck(L_4);
		float L_5 = CapsuleCollider_get_radius_m842698081(L_4, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_6, (0.5f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_2, L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray__ctor_m3379034047((&V_0), L_8, L_9, /*hidden argument*/NULL);
		float L_10 = __this->get_m_CapsuleHeight_18();
		CapsuleCollider_t720607407 * L_11 = __this->get_m_Capsule_20();
		NullCheck(L_11);
		float L_12 = CapsuleCollider_get_radius_m842698081(L_11, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_10-(float)((float)((float)L_12*(float)(0.5f)))));
		Ray_t2469606224  L_13 = V_0;
		CapsuleCollider_t720607407 * L_14 = __this->get_m_Capsule_20();
		NullCheck(L_14);
		float L_15 = CapsuleCollider_get_radius_m842698081(L_14, /*hidden argument*/NULL);
		float L_16 = V_1;
		bool L_17 = Physics_SphereCast_m402220965(NULL /*static, unused*/, L_13, ((float)((float)L_15*(float)(0.5f))), L_16, (-1), 1, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0085;
		}
	}
	{
		__this->set_m_Crouching_21((bool)1);
	}

IL_0085:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::UpdateAnimator(UnityEngine.Vector3)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2866850335;
extern Il2CppCodeGenString* _stringLiteral580826201;
extern Il2CppCodeGenString* _stringLiteral3993866332;
extern Il2CppCodeGenString* _stringLiteral2955053140;
extern Il2CppCodeGenString* _stringLiteral842948034;
extern Il2CppCodeGenString* _stringLiteral1758671042;
extern const uint32_t ThirdPersonCharacter_UpdateAnimator_m1348176094_MetadataUsageId;
extern "C"  void ThirdPersonCharacter_UpdateAnimator_m1348176094 (ThirdPersonCharacter_t1249311527 * __this, Vector3_t2243707580  ___move0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonCharacter_UpdateAnimator_m1348176094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	AnimatorStateInfo_t2577870592  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	int32_t G_B5_0 = 0;
	{
		Animator_t69676727 * L_0 = __this->get_m_Animator_11();
		float L_1 = __this->get_m_ForwardAmount_16();
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Animator_SetFloat_m2537879907(L_0, _stringLiteral2866850335, L_1, (0.1f), L_2, /*hidden argument*/NULL);
		Animator_t69676727 * L_3 = __this->get_m_Animator_11();
		float L_4 = __this->get_m_TurnAmount_15();
		float L_5 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Animator_SetFloat_m2537879907(L_3, _stringLiteral580826201, L_4, (0.1f), L_5, /*hidden argument*/NULL);
		Animator_t69676727 * L_6 = __this->get_m_Animator_11();
		bool L_7 = __this->get_m_Crouching_21();
		NullCheck(L_6);
		Animator_SetBool_m2305662531(L_6, _stringLiteral3993866332, L_7, /*hidden argument*/NULL);
		Animator_t69676727 * L_8 = __this->get_m_Animator_11();
		bool L_9 = __this->get_m_IsGrounded_12();
		NullCheck(L_8);
		Animator_SetBool_m2305662531(L_8, _stringLiteral2955053140, L_9, /*hidden argument*/NULL);
		bool L_10 = __this->get_m_IsGrounded_12();
		if (L_10)
		{
			goto IL_009a;
		}
	}
	{
		Animator_t69676727 * L_11 = __this->get_m_Animator_11();
		Rigidbody_t4233889191 * L_12 = __this->get_m_Rigidbody_10();
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Rigidbody_get_velocity_m2022666970(L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		float L_14 = (&V_0)->get_y_2();
		NullCheck(L_11);
		Animator_SetFloat_m956158019(L_11, _stringLiteral842948034, L_14, /*hidden argument*/NULL);
	}

IL_009a:
	{
		Animator_t69676727 * L_15 = __this->get_m_Animator_11();
		NullCheck(L_15);
		AnimatorStateInfo_t2577870592  L_16 = Animator_GetCurrentAnimatorStateInfo_m1931338898(L_15, 0, /*hidden argument*/NULL);
		V_2 = L_16;
		float L_17 = AnimatorStateInfo_get_normalizedTime_m1330221276((&V_2), /*hidden argument*/NULL);
		float L_18 = __this->get_m_RunCycleLegOffset_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_19 = Mathf_Repeat_m943844734(NULL /*static, unused*/, ((float)((float)L_17+(float)L_18)), (1.0f), /*hidden argument*/NULL);
		V_1 = L_19;
		float L_20 = V_1;
		if ((!(((float)L_20) < ((float)(0.5f)))))
		{
			goto IL_00d1;
		}
	}
	{
		G_B5_0 = 1;
		goto IL_00d2;
	}

IL_00d1:
	{
		G_B5_0 = (-1);
	}

IL_00d2:
	{
		float L_21 = __this->get_m_ForwardAmount_16();
		V_3 = ((float)((float)(((float)((float)G_B5_0)))*(float)L_21));
		bool L_22 = __this->get_m_IsGrounded_12();
		if (!L_22)
		{
			goto IL_00f7;
		}
	}
	{
		Animator_t69676727 * L_23 = __this->get_m_Animator_11();
		float L_24 = V_3;
		NullCheck(L_23);
		Animator_SetFloat_m956158019(L_23, _stringLiteral1758671042, L_24, /*hidden argument*/NULL);
	}

IL_00f7:
	{
		bool L_25 = __this->get_m_IsGrounded_12();
		if (!L_25)
		{
			goto IL_0129;
		}
	}
	{
		float L_26 = Vector3_get_magnitude_m860342598((&___move0), /*hidden argument*/NULL);
		if ((!(((float)L_26) > ((float)(0.0f)))))
		{
			goto IL_0129;
		}
	}
	{
		Animator_t69676727 * L_27 = __this->get_m_Animator_11();
		float L_28 = __this->get_m_AnimSpeedMultiplier_8();
		NullCheck(L_27);
		Animator_set_speed_m3511108817(L_27, L_28, /*hidden argument*/NULL);
		goto IL_0139;
	}

IL_0129:
	{
		Animator_t69676727 * L_29 = __this->get_m_Animator_11();
		NullCheck(L_29);
		Animator_set_speed_m3511108817(L_29, (1.0f), /*hidden argument*/NULL);
	}

IL_0139:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::HandleAirborneMovement()
extern "C"  void ThirdPersonCharacter_HandleAirborneMovement_m4276350424 (ThirdPersonCharacter_t1249311527 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	ThirdPersonCharacter_t1249311527 * G_B2_0 = NULL;
	ThirdPersonCharacter_t1249311527 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	ThirdPersonCharacter_t1249311527 * G_B3_1 = NULL;
	{
		Vector3_t2243707580  L_0 = Physics_get_gravity_m195764919(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_m_GravityMultiplier_5();
		Vector3_t2243707580  L_2 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Physics_get_gravity_m195764919(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Rigidbody_t4233889191 * L_5 = __this->get_m_Rigidbody_10();
		Vector3_t2243707580  L_6 = V_0;
		NullCheck(L_5);
		Rigidbody_AddForce_m2836187433(L_5, L_6, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_7 = __this->get_m_Rigidbody_10();
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Rigidbody_get_velocity_m2022666970(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = (&V_1)->get_y_2();
		G_B1_0 = __this;
		if ((!(((float)L_9) < ((float)(0.0f)))))
		{
			G_B2_0 = __this;
			goto IL_0050;
		}
	}
	{
		float L_10 = __this->get_m_OrigGroundCheckDistance_13();
		G_B3_0 = L_10;
		G_B3_1 = G_B1_0;
		goto IL_0055;
	}

IL_0050:
	{
		G_B3_0 = (0.01f);
		G_B3_1 = G_B2_0;
	}

IL_0055:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_m_GroundCheckDistance_9(G_B3_0);
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::HandleGroundedMovement(System.Boolean,System.Boolean)
extern Il2CppCodeGenString* _stringLiteral3284461968;
extern const uint32_t ThirdPersonCharacter_HandleGroundedMovement_m1707909628_MetadataUsageId;
extern "C"  void ThirdPersonCharacter_HandleGroundedMovement_m1707909628 (ThirdPersonCharacter_t1249311527 * __this, bool ___crouch0, bool ___jump1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonCharacter_HandleGroundedMovement_m1707909628_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimatorStateInfo_t2577870592  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		bool L_0 = ___jump1;
		if (!L_0)
		{
			goto IL_0084;
		}
	}
	{
		bool L_1 = ___crouch0;
		if (L_1)
		{
			goto IL_0084;
		}
	}
	{
		Animator_t69676727 * L_2 = __this->get_m_Animator_11();
		NullCheck(L_2);
		AnimatorStateInfo_t2577870592  L_3 = Animator_GetCurrentAnimatorStateInfo_m1931338898(L_2, 0, /*hidden argument*/NULL);
		V_0 = L_3;
		bool L_4 = AnimatorStateInfo_IsName_m4069203550((&V_0), _stringLiteral3284461968, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0084;
		}
	}
	{
		Rigidbody_t4233889191 * L_5 = __this->get_m_Rigidbody_10();
		Rigidbody_t4233889191 * L_6 = __this->get_m_Rigidbody_10();
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Rigidbody_get_velocity_m2022666970(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = (&V_1)->get_x_1();
		float L_9 = __this->get_m_JumpPower_4();
		Rigidbody_t4233889191 * L_10 = __this->get_m_Rigidbody_10();
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Rigidbody_get_velocity_m2022666970(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = (&V_2)->get_z_3();
		Vector3_t2243707580  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m2638739322(&L_13, L_8, L_9, L_12, /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody_set_velocity_m2514070071(L_5, L_13, /*hidden argument*/NULL);
		__this->set_m_IsGrounded_12((bool)0);
		Animator_t69676727 * L_14 = __this->get_m_Animator_11();
		NullCheck(L_14);
		Animator_set_applyRootMotion_m635228566(L_14, (bool)0, /*hidden argument*/NULL);
		__this->set_m_GroundCheckDistance_9((0.1f));
	}

IL_0084:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::ApplyExtraTurnRotation()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t ThirdPersonCharacter_ApplyExtraTurnRotation_m2992115070_MetadataUsageId;
extern "C"  void ThirdPersonCharacter_ApplyExtraTurnRotation_m2992115070 (ThirdPersonCharacter_t1249311527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonCharacter_ApplyExtraTurnRotation_m2992115070_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_StationaryTurnSpeed_3();
		float L_1 = __this->get_m_MovingTurnSpeed_2();
		float L_2 = __this->get_m_ForwardAmount_16();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_5 = __this->get_m_TurnAmount_15();
		float L_6 = V_0;
		float L_7 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_Rotate_m4255273365(L_4, (0.0f), ((float)((float)((float)((float)L_5*(float)L_6))*(float)L_7)), (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::OnAnimatorMove()
extern "C"  void ThirdPersonCharacter_OnAnimatorMove_m517265912 (ThirdPersonCharacter_t1249311527 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_m_IsGrounded_12();
		if (!L_0)
		{
			goto IL_0061;
		}
	}
	{
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_0061;
		}
	}
	{
		Animator_t69676727 * L_2 = __this->get_m_Animator_11();
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Animator_get_deltaPosition_m1710146426(L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_m_MoveSpeedMultiplier_7();
		Vector3_t2243707580  L_5 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Rigidbody_t4233889191 * L_8 = __this->get_m_Rigidbody_10();
		NullCheck(L_8);
		Vector3_t2243707580  L_9 = Rigidbody_get_velocity_m2022666970(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = (&V_1)->get_y_2();
		(&V_0)->set_y_2(L_10);
		Rigidbody_t4233889191 * L_11 = __this->get_m_Rigidbody_10();
		Vector3_t2243707580  L_12 = V_0;
		NullCheck(L_11);
		Rigidbody_set_velocity_m2514070071(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0061:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter::CheckGroundStatus()
extern "C"  void ThirdPersonCharacter_CheckGroundStatus_m2508098784 (ThirdPersonCharacter_t1249311527 * __this, const MethodInfo* method)
{
	RaycastHit_t87180320  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_2, (0.1f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Vector3_get_down_m2372302126(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = __this->get_m_GroundCheckDistance_9();
		bool L_7 = Physics_Raycast_m2994111303(NULL /*static, unused*/, L_4, L_5, (&V_0), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		Vector3_t2243707580  L_8 = RaycastHit_get_normal_m817665579((&V_0), /*hidden argument*/NULL);
		__this->set_m_GroundNormal_17(L_8);
		__this->set_m_IsGrounded_12((bool)1);
		Animator_t69676727 * L_9 = __this->get_m_Animator_11();
		NullCheck(L_9);
		Animator_set_applyRootMotion_m635228566(L_9, (bool)1, /*hidden argument*/NULL);
		goto IL_0079;
	}

IL_005b:
	{
		__this->set_m_IsGrounded_12((bool)0);
		Vector3_t2243707580  L_10 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_GroundNormal_17(L_10);
		Animator_t69676727 * L_11 = __this->get_m_Animator_11();
		NullCheck(L_11);
		Animator_set_applyRootMotion_m635228566(L_11, (bool)0, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::.ctor()
extern "C"  void ThirdPersonUserControl__ctor_m642427924 (ThirdPersonUserControl_t3088401398 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisThirdPersonCharacter_t1249311527_m1051986587_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1845810996;
extern Il2CppCodeGenString* _stringLiteral884413085;
extern const uint32_t ThirdPersonUserControl_Start_m1434713284_MetadataUsageId;
extern "C"  void ThirdPersonUserControl_Start_m1434713284 (ThirdPersonUserControl_t3088401398 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonUserControl_Start_m1434713284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Camera_t189460977 * L_2 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		__this->set_m_Cam_3(L_3);
		goto IL_002f;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1845810996, /*hidden argument*/NULL);
	}

IL_002f:
	{
		ThirdPersonCharacter_t1249311527 * L_4 = Component_GetComponent_TisThirdPersonCharacter_t1249311527_m1051986587(__this, /*hidden argument*/Component_GetComponent_TisThirdPersonCharacter_t1249311527_m1051986587_MethodInfo_var);
		__this->set_m_Character_2(L_4);
		GPSloc_t2883615338 * L_5 = __this->get_gpsloc_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0056;
		}
	}
	{
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral884413085, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral842948034;
extern const uint32_t ThirdPersonUserControl_Update_m2794896777_MetadataUsageId;
extern "C"  void ThirdPersonUserControl_Update_m2794896777 (ThirdPersonUserControl_t3088401398 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonUserControl_Update_m2794896777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_Jump_6();
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButtonDown_m2792523731(NULL /*static, unused*/, _stringLiteral842948034, /*hidden argument*/NULL);
		__this->set_m_Jump_6(L_1);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl::FixedUpdate()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ThirdPersonUserControl_FixedUpdate_m3260304719_MetadataUsageId;
extern "C"  void ThirdPersonUserControl_FixedUpdate_m3260304719 (ThirdPersonUserControl_t3088401398 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonUserControl_FixedUpdate_m3260304719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		GPSloc_t2883615338 * L_0 = __this->get_gpsloc_7();
		NullCheck(L_0);
		double L_1 = L_0->get_transLat_8();
		V_0 = (((float)((float)L_1)));
		GPSloc_t2883615338 * L_2 = __this->get_gpsloc_7();
		NullCheck(L_2);
		double L_3 = L_2->get_transLong_9();
		V_1 = (((float)((float)L_3)));
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)99), /*hidden argument*/NULL);
		V_2 = L_4;
		Transform_t3275118058 * L_5 = __this->get_m_Cam_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0092;
		}
	}
	{
		Transform_t3275118058 * L_7 = __this->get_m_Cam_3();
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_forward_m1833488937(L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = Vector3_Scale_m1087116865(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		Vector3_t2243707580  L_11 = Vector3_get_normalized_m936072361((&V_3), /*hidden argument*/NULL);
		__this->set_m_CamForward_4(L_11);
		float L_12 = V_1;
		Vector3_t2243707580  L_13 = __this->get_m_CamForward_4();
		Vector3_t2243707580  L_14 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		float L_15 = V_0;
		Transform_t3275118058 * L_16 = __this->get_m_Cam_3();
		NullCheck(L_16);
		Vector3_t2243707580  L_17 = Transform_get_right_m440863970(L_16, /*hidden argument*/NULL);
		Vector3_t2243707580  L_18 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_14, L_18, /*hidden argument*/NULL);
		__this->set_m_Move_5(L_19);
		goto IL_00b3;
	}

IL_0092:
	{
		float L_20 = V_1;
		Vector3_t2243707580  L_21 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		float L_23 = V_0;
		Vector3_t2243707580  L_24 = Vector3_get_right_m1884123822(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		Vector3_t2243707580  L_26 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_22, L_25, /*hidden argument*/NULL);
		__this->set_m_Move_5(L_26);
	}

IL_00b3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_27 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)304), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00d8;
		}
	}
	{
		Vector3_t2243707580  L_28 = __this->get_m_Move_5();
		Vector3_t2243707580  L_29 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_28, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Move_5(L_29);
	}

IL_00d8:
	{
		ThirdPersonCharacter_t1249311527 * L_30 = __this->get_m_Character_2();
		Vector3_t2243707580  L_31 = __this->get_m_Move_5();
		bool L_32 = V_2;
		bool L_33 = __this->get_m_Jump_6();
		NullCheck(L_30);
		ThirdPersonCharacter_Move_m1991112899(L_30, L_31, L_32, L_33, /*hidden argument*/NULL);
		__this->set_m_Jump_6((bool)0);
		return;
	}
}
// System.Void UpdateUserName::.ctor()
extern "C"  void UpdateUserName__ctor_m2201865142 (UpdateUserName_t1355389473 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UpdateUserName::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1416507112;
extern const uint32_t UpdateUserName_Start_m143587690_MetadataUsageId;
extern "C"  void UpdateUserName_Start_m143587690 (UpdateUserName_t1355389473 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UpdateUserName_Start_m143587690_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ButtonPresses_t321815673 * L_0 = __this->get_info_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral1416507112, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UpdateUserName::Update()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3538157094;
extern const uint32_t UpdateUserName_Update_m4053309911_MetadataUsageId;
extern "C"  void UpdateUserName_Update_m4053309911 (UpdateUserName_t1355389473 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UpdateUserName_Update_m4053309911_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ButtonPresses_t321815673 * L_0 = __this->get_info_2();
		NullCheck(L_0);
		bool L_1 = L_0->get_textName_2();
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Text_t356221433 * L_2 = __this->get_textField_3();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, _stringLiteral3538157094);
		goto IL_0041;
	}

IL_0025:
	{
		Text_t356221433 * L_3 = __this->get_textField_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)0, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
