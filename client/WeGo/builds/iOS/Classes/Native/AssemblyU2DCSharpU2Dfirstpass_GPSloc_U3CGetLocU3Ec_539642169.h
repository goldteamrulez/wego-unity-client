﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GPSloc
struct GPSloc_t2883615338;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GPSloc/<GetLoc>c__Iterator1
struct  U3CGetLocU3Ec__Iterator1_t539642169  : public Il2CppObject
{
public:
	// System.Int32 GPSloc/<GetLoc>c__Iterator1::<maxWait>__0
	int32_t ___U3CmaxWaitU3E__0_0;
	// GPSloc GPSloc/<GetLoc>c__Iterator1::$this
	GPSloc_t2883615338 * ___U24this_1;
	// System.Object GPSloc/<GetLoc>c__Iterator1::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean GPSloc/<GetLoc>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 GPSloc/<GetLoc>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CmaxWaitU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetLocU3Ec__Iterator1_t539642169, ___U3CmaxWaitU3E__0_0)); }
	inline int32_t get_U3CmaxWaitU3E__0_0() const { return ___U3CmaxWaitU3E__0_0; }
	inline int32_t* get_address_of_U3CmaxWaitU3E__0_0() { return &___U3CmaxWaitU3E__0_0; }
	inline void set_U3CmaxWaitU3E__0_0(int32_t value)
	{
		___U3CmaxWaitU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetLocU3Ec__Iterator1_t539642169, ___U24this_1)); }
	inline GPSloc_t2883615338 * get_U24this_1() const { return ___U24this_1; }
	inline GPSloc_t2883615338 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(GPSloc_t2883615338 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetLocU3Ec__Iterator1_t539642169, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetLocU3Ec__Iterator1_t539642169, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetLocU3Ec__Iterator1_t539642169, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
