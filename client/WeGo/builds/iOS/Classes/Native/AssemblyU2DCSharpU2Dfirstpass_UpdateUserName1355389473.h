﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ButtonPresses
struct ButtonPresses_t321815673;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateUserName
struct  UpdateUserName_t1355389473  : public MonoBehaviour_t1158329972
{
public:
	// ButtonPresses UpdateUserName::info
	ButtonPresses_t321815673 * ___info_2;
	// UnityEngine.UI.Text UpdateUserName::textField
	Text_t356221433 * ___textField_3;

public:
	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(UpdateUserName_t1355389473, ___info_2)); }
	inline ButtonPresses_t321815673 * get_info_2() const { return ___info_2; }
	inline ButtonPresses_t321815673 ** get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(ButtonPresses_t321815673 * value)
	{
		___info_2 = value;
		Il2CppCodeGenWriteBarrier(&___info_2, value);
	}

	inline static int32_t get_offset_of_textField_3() { return static_cast<int32_t>(offsetof(UpdateUserName_t1355389473, ___textField_3)); }
	inline Text_t356221433 * get_textField_3() const { return ___textField_3; }
	inline Text_t356221433 ** get_address_of_textField_3() { return &___textField_3; }
	inline void set_textField_3(Text_t356221433 * value)
	{
		___textField_3 = value;
		Il2CppCodeGenWriteBarrier(&___textField_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
