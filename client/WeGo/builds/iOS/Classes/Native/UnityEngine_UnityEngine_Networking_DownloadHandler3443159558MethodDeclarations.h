﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t3443159558;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
struct DownloadHandlerBuffer_t3443159558_marshaled_pinvoke;
struct DownloadHandlerBuffer_t3443159558_marshaled_com;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.DownloadHandlerBuffer::.ctor()
extern "C"  void DownloadHandlerBuffer__ctor_m1363181913 (DownloadHandlerBuffer_t3443159558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Networking.DownloadHandlerBuffer::GetData()
extern "C"  ByteU5BU5D_t3397334013* DownloadHandlerBuffer_GetData_m387234843 (DownloadHandlerBuffer_t3443159558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.DownloadHandlerBuffer::GetText()
extern "C"  String_t* DownloadHandlerBuffer_GetText_m1584160271 (DownloadHandlerBuffer_t3443159558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Networking.DownloadHandlerBuffer::InternalGetData()
extern "C"  ByteU5BU5D_t3397334013* DownloadHandlerBuffer_InternalGetData_m4040620008 (DownloadHandlerBuffer_t3443159558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.DownloadHandlerBuffer::InternalGetText()
extern "C"  String_t* DownloadHandlerBuffer_InternalGetText_m593854464 (DownloadHandlerBuffer_t3443159558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct DownloadHandlerBuffer_t3443159558;
struct DownloadHandlerBuffer_t3443159558_marshaled_pinvoke;

extern "C" void DownloadHandlerBuffer_t3443159558_marshal_pinvoke(const DownloadHandlerBuffer_t3443159558& unmarshaled, DownloadHandlerBuffer_t3443159558_marshaled_pinvoke& marshaled);
extern "C" void DownloadHandlerBuffer_t3443159558_marshal_pinvoke_back(const DownloadHandlerBuffer_t3443159558_marshaled_pinvoke& marshaled, DownloadHandlerBuffer_t3443159558& unmarshaled);
extern "C" void DownloadHandlerBuffer_t3443159558_marshal_pinvoke_cleanup(DownloadHandlerBuffer_t3443159558_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct DownloadHandlerBuffer_t3443159558;
struct DownloadHandlerBuffer_t3443159558_marshaled_com;

extern "C" void DownloadHandlerBuffer_t3443159558_marshal_com(const DownloadHandlerBuffer_t3443159558& unmarshaled, DownloadHandlerBuffer_t3443159558_marshaled_com& marshaled);
extern "C" void DownloadHandlerBuffer_t3443159558_marshal_com_back(const DownloadHandlerBuffer_t3443159558_marshaled_com& marshaled, DownloadHandlerBuffer_t3443159558& unmarshaled);
extern "C" void DownloadHandlerBuffer_t3443159558_marshal_com_cleanup(DownloadHandlerBuffer_t3443159558_marshaled_com& marshaled);
